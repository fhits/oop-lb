#include <string>

#pragma once
class IStringable
{
	virtual std::string to_string() = 0;

};

