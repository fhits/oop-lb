#pragma once
#include <stdexcept>
template <class T, int range>
class LimitedValue
{
private:
	T value;
public:
	const T getValue() const { return this->value; }
	void setValue(T val) {
		if (val >= range || val < 0) {
			throw std::out_of_range("Value out of range!");
		}

		this->value = val;
	}
};

