#pragma once
#include "Time.h"
#include <cmath>
#include <string>

namespace AMTime {

	class Swatch : public Time
	{
	private: 
		int beats{ 0 };
		double result{ 0 };
	public:

		std::string to_string() {
			return "@" + this->result;
		}

		Swatch(int beats) : beats(beats) {
			double wholeDay = 1000;
			double result = beats / wholeDay; // 250 / 1000 -> 0.25 * 24 -> 6.42582
			result *= 24;
			this->result = result;
			double hours, frac, minutes, seconds;
			minutes = std::modf(result, &hours);
			minutes *= 60;
			seconds = std::modf(minutes, &frac);
			seconds *= seconds * 60;

			this->setHours(hours);
			this->setMinutes(minutes);
			this->setSeconds(seconds);
		};

		Swatch() : Time() { };

	};

}

