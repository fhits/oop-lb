#pragma once
#include <ctime>
#include <iostream>
#include "LimitedValue.h"
#include "IStringable.h"

namespace AMTime {
	typedef LimitedValue<double, 24> hourType;
	typedef LimitedValue<double, 60> otherType;

	class Time : public IStringable
	{
	protected:
		hourType hours;
		otherType minutes, seconds;

	public:
		std::string to_string() {
			return "sers";
			//std::string result = this->getHours() + "" + ":" + "" + this->getMinutes() + ":" + this->getSeconds() + std::endl;
		}

		Time(double hours, double minutes, double seconds) {
			this->hours.setValue(hours);
			this->minutes.setValue(minutes);
			this->seconds.setValue(seconds);
		};

		Time() {
			const std::time_t t = std::time(nullptr);
			std::tm now;
			localtime_s(&now, &t);

			this->setHours(now.tm_hour);
			this->setMinutes(now.tm_min);
			this->setSeconds(now.tm_sec);
		};

		double const getHours() { return this->hours.getValue(); }
		double const getMinutes() { return this->minutes.getValue(); }
		double const getSeconds() { return this->seconds.getValue(); }

		void setHours(double hours) {
			this->hours.setValue(hours);
		}
		void setMinutes(double minutes) {
			this->hours.setValue(minutes);
		}
		void setSeconds(double seconds) {
			this->hours.setValue(seconds);
		}



	};
}
