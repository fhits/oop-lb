#include <iostream>
#include <stdexcept>
#include <string>
#include <cassert>
#include <vector>
#include <memory>
#include "LimitedValue.h"
#include "Time.h"
#include "Swatch.h"

void createLimitedValue();
void createLimitedValueOutofRange();
void createBaseTime();
void createSwatchTime();

int main()
{
	try {
		/*createLimitedValue();
		createBaseTime();*/
		createSwatchTime();
		//createLimitedValueOutofRange();
	}
	catch (std::out_of_range& e) {
		std::cerr << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	return 0;
}

void createLimitedValue() {

	LimitedValue<int, 24> lV;
	lV.setValue(10);
	assert(lV.getValue() == 10, "Values do not match!");
}

void createLimitedValueOutofRange() {

	LimitedValue<int, 24> lV;
	lV.setValue(27);
	assert(lV.getValue() == 10, "Values do not match!");
}

void createBaseTime() {

	AM::Time testTime = AM::Time(22, 22, 12);

	assert(testTime.getHours() == 22, "Hours do not match!");
	assert(testTime.getMinutes() == 22, "Minutes do not match!");
	assert(testTime.getSeconds() == 12, "Seconds do not match!");

}

void createSwatchTime() {

	AMTime::Swatch swatch = AMTime::Swatch(250);

	assert(swatch.getHours() == 6, "Hours do not match!");
	//std::cout << swatch.getHours() << std::endl;
	//std::cout << swatch.getMinutes() << std::endl;
	//std::cout << swatch.getSeconds() << std::endl;

}

void createContainer() {
	//
	//std::vector < std::shared_ptr<AM::Time>> times;

	//typedef std::make_shared<AM::Time> timeShared;
	//AM::Swatch testSwatch = AM::Swatch();
	//times.push_back(std::make_shared<AM::Time>(testSwatch));
	//AM::Time testTime = AM::Time();
	//times.push_back(std::make_shared<AM::Time>(testSwatch));
	//AM::Swatch testSwatch = AM::Swatch();
	//AM::Swatch testSwatch = AM::Swatch();

}
