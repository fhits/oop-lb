#pragma once
#include <string>
class Email
{
private:
	std::string email{ "" };
public:
	Email() = default;
	Email(std::string email) : email{ email } {};

	const std::string getEmail() const { return this->email; }
	void setEmail(std::string mail) {
		this->email = mail;
	}
};

