#include "Person.h"

std::ostream& operator<<(std::ostream& stream, const Person& person) {
	return person.output(stream);
}
