#pragma once
#include <iostream>
#include <algorithm>
#include <string>
#include "Email.h"
#include "PrintExt.h"

enum class Gender {

	male = 0,
	female,
	diverse

};

class Person : public Email
{
protected:
	std::string name;
	int birthDay;
	Gender gender;
	std::string email;

public:
	Person() = delete;
	Person(std::string name, int birthDay, Gender gender) : name{ name }, birthDay{ birthDay }, gender{ gender }, Email() {};
	Person(std::string name, int birthDay, Gender gender, std::string email) : name{ name }, birthDay{ birthDay }, gender{ gender }, Email(email) {};

	friend std::ostream& operator<<(std::ostream& stream, const Person& person);

	virtual std::ostream& output(std::ostream& stream) const {
		char gender = 'x';

		switch (this->gender) {
		case Gender::male: gender = 'm';
			break;
		case Gender::female: gender = 'f';
			break;
		case Gender::diverse: gender = 'd';
			break;
		default: break;
		}
		std::string genderStr(1, gender);

		stream << "{ ";
		stream << PrintExt::to_PrintItemStr("name", this->name);
		stream << ", ";
		stream << PrintExt::to_PrintItemStr("birthdate", this->birthDay);
		stream << ", ";
		stream << PrintExt::to_PrintItemStr("Gender", genderStr);
		stream << " }" << std::endl;
		return stream;
	}
	std::string getUpperName() {
		size_t found = this->name.find(',');

		std::string lastName, firstName{ "" };
		if (found != std::string::npos) {
			lastName = this->name.substr(0, found);
			firstName = this->name.substr(found + 1);
		}
		std::transform(lastName.begin(), lastName.end(), lastName.begin(), [](char c) -> char { return std::toupper(c);  });
		return this->name;
	}

};

