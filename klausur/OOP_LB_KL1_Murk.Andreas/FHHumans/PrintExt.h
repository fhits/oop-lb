#pragma once
#include <string>
#include <sstream>
#include <iostream>

namespace PrintExt {

	template <typename T>
	static std::string to_PrintItemStr(std::string name, T value) {
		if (std::is_arithmetic_v<T>) {
			return "\"" + name + "\"" + "=" + std::to_string(value);
		}
	}

	template <>
	static std::string to_PrintItemStr<std::string>(std::string name, std::string value) {
		return "\"" + name + "\"" + "=" + "\"" + value + "\"";
	}

};
