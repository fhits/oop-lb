#pragma once
#include <iostream>
#include "Person.h"
class Student :
	public Person
{
private:
	int mNr{ 0 };

	const int getDigits(long int number) const {
		int count = 0;
		auto temp = number;

		while (number != 0) {
			count++;
			number = number / 10;
		}
		return count;
	}
public:
	Student(std::string name, int birthDay, Gender gender, int matrkNr) : Person(name, birthDay, gender) {
		if (matrkNr <= 1000000000 || matrkNr >= 9999999999) {
			throw std::invalid_argument("Matrikelnummer ist zu gross!");
		}
		this->mNr = matrkNr;
	}

	Student(std::string name, int birthDay, Gender gender, std::string email, long int matrkNr) : Person(name, birthDay, gender, email) {
		if (matrkNr <= 1000000000 || matrkNr >= 9999999999) {
			throw std::invalid_argument("Matrikelnummer ist zu gross!");
		}
		this->mNr = matrkNr;
	}

	virtual std::ostream& output(std::ostream& stream) const override {
		stream << "Student = ";
		Person::output(stream);
		stream << "{ " << PrintExt::to_PrintItemStr("Matnr", this->mNr) << ", " << PrintExt::to_PrintItemStr("email", this->email) << "}" << std::endl;
		return stream;
	}
};

