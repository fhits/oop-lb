#pragma once
#include <iostream>
#include <ostream>
#include <string>
#include "Person.h"

class Teacher :
	public Person
{
protected:
	int svNr{ 0 };

public:
	const std::string getExtSVNr() const {
		std::ostringstream os;
		std::string birthStr = std::to_string(this->birthDay);
		os << this->svNr << "-" << birthStr.substr(6, 2) << birthStr.substr(4, 2) << birthStr.substr(2, 2);
		return os.str();
	}

	Teacher(std::string name, int birthDay, Gender gender, int svNr) : Person(name, birthDay, gender) {
		if (sizeof(svNr) > 10) {
			throw std::invalid_argument("SvNr zu gross!");
		}
		this->svNr = svNr;
	}

	Teacher(std::string name, int birthDay, Gender gender, std::string email, int svNr) : Person(name, birthDay, gender, email) {
		if (sizeof(svNr) > 10) {
			throw std::invalid_argument("SvNr zu gross!");
		}
		this->svNr = svNr;
	}

	virtual std::ostream& output(std::ostream& stream) const override {
		stream << "Teacher = ";
		Person::output(stream);
		stream << "{ " << PrintExt::to_PrintItemStr("SVNr", this->svNr) << ", " << PrintExt::to_PrintItemStr("email", this->email) << "}" << std::endl;
		return stream;
	}
};

