﻿#include <cassert>
#include <vector>
#include <memory>
#include <algorithm>
#include <iostream>
#include "Person.h"
#include "Student.h"
#include "Teacher.h"


// FHHumans.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//void check_email_validation(const std::string& s)
//{
//	// Aufruf der Email Validierungsfunktion und Abfangen einer m�glichen 
//	// Exception zur Ausgabe einer Fehlermeldung auf cerr
//	// INSERT YOUR CODE HERE!
//}



int main()
{
	try {
		// -------------------------------------------------------------------
		// // Person instanziieren
		Person  p1("Mustermann, Max", 19770707, Gender::male);
		// // Student instanziieren
		Student s1("Example, Eva Maria", 19970217, Gender::female, "evae@gmail.com", 1910581005);
		// // Teacher instanziieren
		Teacher t1("Tempo, Theox", 20010605, Gender::diverse, "ttempo@fh.co.at", 8976);
		// // Einf�gen eines Studenten s2 mit den eigenen Daten
		// INSERT YOUR CODE HERE!
		Student s2("Andreas, Murk", 19962403, Gender::male, "andimurk@hotmail.de", 1919191919);
		// -------------------------------------------------------------------

		// Testen der Methode getExtSVNr
		// INSERT YOUR CODE HERE!
		std::cout << t1.getExtSVNr() << std::endl;

		// Testen der Klasse Email mit Getter und Setter
		// INSERT YOUR CODE HERE!
		Email email("test@example.com");
		std::cout << email.getEmail() << std::endl;


		// -------------------------------------------------------------------
		// Testen der Function to_PrintItemStr aus dem Namensraum PrintExt
		// INSERT YOUR CODE HERE!
		std::cout << PrintExt::to_PrintItemStr("value", 1234) << std::endl;
		std::cout << PrintExt::to_PrintItemStr("value", std::string("Maxi, Maxiking")) << std::endl;


		// -------------------------------------------------------------------
		//  Container f�r Person, Studenten und Teacher bef�llen
		// INSERT YOUR CODE HERE!
		std::vector<std::shared_ptr<Person>> persons;
		std::shared_ptr<Person> pp1 = std::make_shared<Person>(p1);
		persons.push_back(pp1);
		std::shared_ptr<Student> ss1 = std::make_shared<Student>(s1);
		persons.push_back(ss1);
		std::shared_ptr<Teacher> tt1 = std::make_shared<Teacher>(t1);
		persons.push_back(tt1);


		// -------------------------------------------------------------------
		// Wir testen die operator << Methode einzeln f�r jeden Typ
		//std::cout << p1 << std::endl;
		//std::cout << s1 << std::endl;
		//std::cout << t1 << std::endl;

		// -------------------------------------------------------------------
		// Geben Sie in einer Schleife �ber alle Elemente des Container die 
		// einzelnen Elemente so auf cout aus, dass eine Ausgabe passend f�r 
		// den jeweils zugrundeliegenden Typ erfolgt.
		// INSERT YOUR CODE HERE!
		for (auto&  item : persons) {
			std::cout << *item;
		}

		// -------------------------------------------------------------------
		// Alle Nachname in der Collection mit Hilfe von transform() und einer 
		// lambda function in Gro�buchstaben konvertieren
		// INSERT YOUR CODE HERE!
		//     std::transform(ifirst, ilast, ofirst, lambda_function);


		// -------------------------------------------------------------------
		// Email validieren �ber eigene kleine Validierungsfunktion
		//check_email_validation("roland.graf@fh-salzburg.ac.at");
		//check_email_validation("@fh-salzburg.ac.at");
		//check_email_validation("roland.graf.fh-salzburg.ac.at");
		//check_email_validation("roland.graf@");


	}
	catch (std::invalid_argument& e) {
		std::cerr << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	std::cout << "Bye!\n";
}
