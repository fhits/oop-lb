#include <iostream>
#include <stdexcept>
#include "Array2D.h"
void create2DArray();

int main()
{
	std::cout << "Hello World!\n";
	try {
		create2DArray();
	}
	catch (std::out_of_range& e) {
		std::cerr << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	return EXIT_SUCCESS;
}

void create2DArray() {
	Array2D<int, 2, 2> arr({ 1,2 }, { 1,2 });
}



}
