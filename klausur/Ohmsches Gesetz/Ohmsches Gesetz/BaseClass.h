#pragma once
#include <iostream>
template <class T>
class BaseClass
{
protected:
	T value;
	BaseClass() = delete;
	BaseClass(const T& value) {
		if (!std::is_arithmetic_v<T>) {
			throw std::invalid_argument("no integral type provided!");
		}
		this->value = value;
	}

public:
	const T getValue() const { return this->value; }

};

