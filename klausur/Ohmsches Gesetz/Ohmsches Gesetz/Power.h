#pragma once
#include "BaseClass.h"
#include <stdexcept>

using BaseType = double;

class Power : public BaseClass<BaseType> {


public:
	Power() = delete;
	Power(BaseType value) :BaseClass(value) { };

	bool operator==(const Power& other) const {
		if (this->value == other.value) return true;
		return false;
	}

	Power operator+(Power& other) {
		Power newRes(this->value
			+ other.value);
		return newRes;
	}
	Power operator-(Power& other) {
		Power newRes(this->value
			- other.value);
		return newRes;
	}
	Power operator*(Power& other) {
		Power newRes(this->value
			* other.value);
		return newRes;
	}

	Power operator/(Power& other) {
		if (other.value == 0) throw std::overflow_error("divide by zero exception!");
		auto newVal = this->value / other.value;

		Power newRes(this->value
			/ other.value);
		return newRes;
	}

};

