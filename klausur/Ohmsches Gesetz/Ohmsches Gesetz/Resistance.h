#pragma once
#include "Voltage.h"
#include "Power.h"
#include "BaseClass.h"

class Resistance : public BaseClass<BaseType> {

public:
	Resistance() = default;
	Resistance(BaseType value) : BaseClass(value) { };

	bool operator==(const Resistance& other) const {
		if (this->value == other.value) return true;
		return false;
	}

	Resistance operator+(Resistance& other) {
		Resistance newRes(this->value
			+ other.value);
		return newRes;
	}
	Resistance operator-(Resistance& other) {
		Resistance newRes(this->value
			- other.value);
		return newRes;
	}
	Resistance operator*(Resistance& other) {
		Resistance newRes(this->value
			* other.value);
		return newRes;
	}

	Resistance operator/(Resistance& other) {
		if (other.value == 0) throw std::overflow_error("divide by zero exception!");
		BaseType newVal = this->value / other.value;

		Resistance newRes(this->value
			/ other.value);
		return newRes;
	}

};


