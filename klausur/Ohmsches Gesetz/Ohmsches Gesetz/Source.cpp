#include <iostream>
#include <stdexcept>
#include <cmath>
#include "Power.h"
#include "Voltage.h"
#include "Resistance.h"

void createResPlus();
void createResDivide();
void voltageResult();
void createEqualOperator();



template <typename T>
const bool is_equal(const T& x, const T& y, int digits) {
	auto f = 1 / std::pow(10, digits);
	return (std::abs(x - y) <= f) || std::abs(x - y) < std::numeric_limits<T>::min();
}

int main()
{
	try
	{
		createResPlus();
		createResDivide();
		voltageResult();
		createEqualOperator();
	}
	catch (const std::invalid_argument& e) {
		std::cerr << e.what() << std::endl;
	}
	catch (const std::overflow_error& e)
	{
		std::cerr << e.what() << std::endl;
	}
	catch (const std::exception&)
	{

	}
	catch (...) {}
	return 0;
}

void createResPlus() {

	Resistance r1(100);
	Resistance r2(200);

	Resistance r3(0);
	r3 = r1 + r2;

	std::cout << r3.getValue() << std::endl;
}
void createResDivide() {

	Resistance r1(100);
	Resistance r2(10);

	Resistance r3(0);
	r3 = r1 / r2;

	std::cout << r3.getValue() << std::endl;
}

void voltageResult() {
	Resistance r1(100);
	Voltage v2(10);

	Power p3 = v2 / r1;

	std::cout << p3.getValue() << std::endl;

}

void createEqualOperator() {
	Resistance r1(100);
	Resistance r2(100);

	std::cout << (r1 == r2) << std::endl;
}