#pragma once
#include "Resistance.h"
#include "Power.h"

class Voltage : public BaseClass<BaseType> {
	
public:
	Voltage() = delete;
	Voltage(BaseType value) : BaseClass(value) { };

	Power operator/(Resistance& res) {
		if (res.getValue() == 0) throw std::overflow_error("divide by zero exception!");
		BaseType newVal = this->getValue() / res.getValue();

		Power newPower(newVal);
		return newPower;
	}
	Voltage operator+(Voltage& other) {
		Voltage newRes(this->value
			+ other.value);
		return newRes;
	}
	Voltage operator-(Voltage& other) {
		Voltage newRes(this->value
			- other.value);
		return newRes;
	}
	Voltage operator*(Voltage& other) {
		Voltage newRes(this->value
			* other.value);
		return newRes;
	}

	Voltage operator/(Voltage& other) {
		if (other.value == 0) throw std::overflow_error("divide by zero exception!");
		BaseType newVal = this->value / other.value;

		Voltage newRes(this->value
			/ other.value);
		return newRes;
	}

	bool operator==(const Voltage& other) const {
		if (this->value == other.value) return true;
		return false;
	}
};

