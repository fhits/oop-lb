#include <array>
#include <iostream>
#include <list>

#include <memory>
#include <string>

struct Person {
	std::string name;
	unsigned ID;
};

Person* doit() {

	Person* p = new Person;
	return p;

}

std::shared_ptr<Person> doitSmart() {
	auto p = std::make_shared<Person>();
	return p;
}

// with & reference call by reference else call by value
size_t foo(std::shared_ptr<Person> &p) {
	p->name = "Marko Foo";
	return p->name.length();
}

int main() {

	Person ps{ "Roland", 1234 };
	
	Person* ph = new Person;
	// do something interesting
	delete ph;

	{
		auto p = doit();
		// do something interesting
		p->name = "Maxiking";
		p->ID = 1234;
	}
	std::shared_ptr<Person> ssp;
	{
		auto sp = doitSmart();
		sp->name = "Lisa yee";
		sp->ID = 112;
		ssp = sp;
	}
	ssp.reset();

	Person p1{ "Hans", 333 };
	// object has to be provided, no pointer - how would shared pointer know, when to call delete?
	std::shared_ptr<Person> sp1 = std::make_shared<Person>(p1);


	Person *p2 = new Person{ "Franz", 333};
	// same as above - provide dereferenced object and no pointer
	std::shared_ptr<Person> sp2 = std::make_shared<Person>(*p2);
	
	// only copy sp2 will be changed - p2 remains the same
	sp2->name = "Hubert";

	// valid - if sp3 out of scope - deconstructor of p2 will be called
	std::shared_ptr<Person> sp3 = std::shared_ptr<Person>(p2);
	sp3.get();
	// p1 not in heap - break application
	//	std::shared_ptr<Person> sp4 = std::shared_ptr<Person>(&p1);
	
	foo(sp2);

	// unique ptr

	std::unique_ptr<Person> up1{ new Person{"Unique", 999 } };
	// gets current data from unique pointer
	auto pp1 = up1.get();

	Person* test = new Person{ "What", 999 };
	std::unique_ptr<Person> up2 = std::make_unique<Person>(*test);

	std::unique_ptr<Person> up3 = std::move(up1);
	// empty - complete data is moved to up3
	auto pp2 = up1.get();
	std::cout << pp2->name << std::endl;
	return 1;
}