enum Tag {

	Mo = 1, Di, Mi, Do, Fr, Sa, So,
};

enum class CTag {

	Mo = 1, Di, Mi, Do, Fr, Sa, So,
	Mon = 1, Tue, Wed ,Thu, Fri , Sat, Sun
};

enum class bitflags {

	right = 1,
	left = 2,
	both = left | right

};

int main() {

	Tag t = Mo;
	CTag ct = CTag::Mo;

	int t_int = t;
	Tag t2 = (Tag)6;
	CTag ct2 = (CTag)9;
	
	bitflags bf = bitflags::both;
}