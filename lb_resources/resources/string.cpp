/* #include <cstdio> */
#include <cstdio>
#include <iostream>
#include <string.h>
#include <utility>

class String {
	private:
	public:
		char* m_Data;
		uint32_t m_Size;


		String() {
			printf("Created!\n");
		};

		String(const char* string) {
			printf("Created!\n");
			m_Size = strlen(string);
			m_Data = new char[m_Size]; 
			memcpy(m_Data, string, m_Size);
		}

		String(const String& other) {
			printf("Copied!\n");
			m_Size = other.m_Size;
			m_Data = new char[m_Size]; 
			memcpy(m_Data, other.m_Data, m_Size);
		}

		String(String&& other) noexcept {
			printf("Moved!\n");
			m_Size = other.m_Size;
			m_Data = other.m_Data; 

			other.m_Data = nullptr;
			other.m_Size = 0;
		}


		~String() {
			printf("Destroyed!\n");
			delete m_Data;
		}

		void print() {
			for (uint32_t i = 0; i < m_Size; i++) {
				printf("%c", m_Data[i]);
			}
			printf("\n");
		}
};

class Account {

	private:
		String m_Name;

	public:
		Account() {
			printf("Created Account!\n");
		}

		Account(const String& name) : m_Name{name} { 
			printf("Created Account with string\n");
		}

		void print() {
			for (uint32_t i = 0; i < m_Name.m_Size; i++) {
				printf("%c", m_Name.m_Data[i]);
			}
			printf("\n");
		}

};


class Entity {

	private:
		String m_Name;
		Account account;

	public:
		Entity() {
			printf("Created Entity!\n");
		}

		Entity(const String& name) : m_Name{name} { 
			printf("Created entity with string!\n");
		}

		Entity(Account account) : account{account} {
			printf("Created entity with account!\n");
		}

		Entity(const String&& name) : m_Name{name} { }

		void printName() {
			m_Name.print();
		}

		~Entity() {
		}

		void printAccount() {
			account.print();
		}

};

int& getValue() {
	static int value = 10;
	return value;
}

void setValue(int& value) {
	std::cout << "lvalue" << std::endl;
}
void setValue(const int& value) {
	std::cout << "const lvalue" << std::endl;
}

void setValue(int&& value) {
	std::cout << "rvalue" << std::endl;
}

int main(int argc, char *argv[])
{
	getValue() = 5;

// lvalue -> value adress
// rvalue -> value

	/* int i = getValue(); */

	int a = 5;
	// do not works
	/* int&& x = 10; */
	// do not works
	/* const int& y = 10; */

	setValue(a);
	setValue(10);

	std::string firstname = "daniel";
	std::string lastName = "ertl";

	std::string name = firstname + lastName;

	/* setValue(10); */

	/* printf("%d", i); */	
	/* Entity entity(String("Andrew")); */
	/* Entity entity("Andrew"); */
	Entity entity = Entity(Account());
	/* Account account = Account(); */

		
	return 0;
}
