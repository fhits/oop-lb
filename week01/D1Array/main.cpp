#include <array>
#include <iostream>
#include <iomanip>

using std::cout;
using std::array;
using std::setw;

auto coutArray(const array<double, 10>& array) {
	for (size_t i = 0; i < array.size(); i++) {
		cout << setw(10);

		if (i % 4 == 0) cout << "\n";

		cout << setw(10) << array[i];
	}
}

int main(int argc, char *argv[])
{
	
	array<double, 10> double_array{ 0 };

	double value = 0;

	for (size_t i = 0; i < double_array.size(); i++) {
		double_array[i] = value;
		value += 0.3456789;
	}
	
	coutArray(double_array);

	return 0;
}
