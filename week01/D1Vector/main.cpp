#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <string>

using namespace std;

auto writeToFile(vector<string>& vector, string fileName) {
	ofstream filePtr;
	filePtr.open(fileName);

	filePtr << "Programmname=" << vector[0] << "\n";
	filePtr << "Argumente=" << vector.size() << "\n";

	for (size_t i = 1; i < vector.size(); i++) {
		filePtr << "Argument_" << i << "=" + vector[i] << "\n";
	}
	filePtr.close();

}

int main(int argc, char* argv[]) {
	vector<string> arguments(argv, argv + argc);

	writeToFile(arguments, "myotherfile.txt");

	return 0;
}