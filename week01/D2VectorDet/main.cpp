#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <array>

using namespace std;

auto readFile(string filePath) {
	vector<double> lines;
	ifstream filePtr(filePath);

	vector<vector<double>> result;

	if (filePtr.is_open()) {
		string line;
		while (getline(filePtr, line, '\n')) {
			lines.clear();
			line.erase(remove(line.begin(), line.end(), ' '), line.end());
			string value;
			stringstream lineStream(line);
			while (getline(lineStream, value, ',')) {
				lines.push_back(stod(value));
			}
			result.push_back(lines);
		}
		filePtr.close();
	}
	else {
		cerr << "cannot open file \n";
		exit(EXIT_FAILURE);
	}
	return result;
}

auto calcDet(vector<vector<double>>& vector) {
	
	double result =
		vector[0][0] * vector[1][1] * vector[2][2]
		+ vector[0][1] * vector[1][2] * vector[2][0]
		+ vector[0][2] * vector[1][0] * vector[2][1] 
		- vector[0][2] * vector[1][1] * vector[2][0]
		- vector[0][0] * vector[1][2] * vector[2][1]
		- vector[0][1] * vector[1][0] * vector[2][2];
	return result;
}

auto printMatrix(vector<vector<double>>& vector) {

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (i == 1 && j == 0) { cout << "det "; }
			cout << "\t" << vector[i][j] << "\t";
			if (i == 1 && j == 2) { cout << "= " << calcDet(vector); }
		}
		cout << "\n";
	}

}
int main(int argc, char* argv[]) {

	if (!argv[1]) {
		cerr << "No filename given!";
		exit(EXIT_FAILURE);
	}
	string fileName = argv[1];
	vector<vector<double>> moreDim(3, vector<double>(3));

	vector<vector<double>> result = readFile(fileName);
	printMatrix(result);
	return EXIT_SUCCESS;
}