#include <cstdlib>
#include <iostream>
#include <vector>
#include <list>
#include <memory>

#define print(msg) std::cout << (msg) << std::endl
#define printm(msg, msg2, ...) std::cout << (msg) << (msg2) << std::endl
#define error(msg) std::cerr << (msg) << std::endl

struct Material {
	unsigned ID;
	int articleNr;
	std::string description;
};

struct Container {
	int containerNr;
	std::list<std::shared_ptr<Material>> materials;
};

struct Warehouse {
	std::vector<Container*> containers;
};

auto createMaterial(unsigned ID, int articleNr, std::string description) {
	auto newMaterial = new Material{ ID, articleNr, description };
	auto sharedPtr = std::shared_ptr<Material>(newMaterial);
	if (!sharedPtr) {
		/* std::cerr << "problem allocating pointer!" << std::endl; */
		error("problem allocating pointer!");
		exit(0);
	}
	return sharedPtr;
}

auto storeIn(unsigned ID, int articleNr, std::string description, Container *container) {
	auto newMaterial = createMaterial(ID, articleNr, description);
	container->materials.push_front(newMaterial);
	printm("Material has been added to Container ", container->containerNr);
	printm("Size of Container is now = ", container->materials.size());
}

auto takeOut(Warehouse &wareHouse, int articleNr) {
  if (wareHouse.containers.empty()) {
    error("error: warehouse seems to be empty!");
    exit(0);
  }
  for (auto container : wareHouse.containers) {
    if (container->materials.empty()) {
      error("error: container seems to be empty!");
      continue;
    }
    if (container->materials.front()->articleNr != articleNr) {
      error("article not found!");
      continue;
    }
    container->materials.pop_front();
    printm("Material has been removed from Container-Front ",
           container->containerNr);
  }
}

auto addContainer(Warehouse& wareHouse, Container* container) {
	wareHouse.containers.push_back(container);
}

auto printWarehouse(const Warehouse &wareHouse) {
  if (wareHouse.containers.empty()) {
		error("no containers found!");
    exit(0);
  }
  printm("Containers found: ", wareHouse.containers.size());
  for (auto container : wareHouse.containers) {
    for (auto material : container->materials) {
      std::cout << "[Container: " << container->containerNr << "]" << std::endl;
      std::cout << "Materials" << std::endl;
      std::cout << "\t[ID: " << material->ID
                << ", articleNr: " << material->articleNr << ", description: '"
                << material->description << "']" << std::endl;
    }
  }
}

int main(int argc, char *argv[]) {
	Warehouse wareHouse = Warehouse{};
	Container *stoneContainer = new Container{1};
	Container *paperContainer = new Container{2};

	addContainer(wareHouse, stoneContainer);
	addContainer(wareHouse, paperContainer);

	storeIn(rand() % 100, 1, "This is copper", stoneContainer);
	storeIn(rand() % 100, 2, "This is paper", stoneContainer);
	storeIn(rand() % 100, 1, "This is paper", paperContainer);

	takeOut(wareHouse, 1);
	takeOut(wareHouse, 1);
	takeOut(wareHouse, 2);
	printWarehouse(wareHouse);

	return EXIT_SUCCESS;
}
