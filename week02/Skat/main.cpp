#include <iostream>
#include <array>
#include <cstdlib>
#include <time.h>
#include <memory>
#include <string>

#define SIZE 32

using namespace std;

enum Color : unsigned int {
	diamonds,
	clubs,
	hearts,
	spades
};

enum Number : unsigned int {
	seven = 7,
	eight,
	nine,
	ten,
	jack,
	queen,
	king,
	ace
};

struct card_t {
	Color color;
	Number number;
} typedef Card;

auto getNumber(int number) {
	switch (number)
	{
	case 7:
		return "seven";
		break;
	case 8:
		return "eight";
		break;
	case 9:
		return "nine";
		break;
	case 10:
		return "ten";
		break;
	case 11:
		return "jack";
		break;
	case 12:
		return "queen";
		break;
	case 13:
		return "king";
		break;
	case 14:
		return "ace";
		break;
	default:
		break;
	}
}

auto getColor(int color) {
	switch (color)
	{
	case 1:
		return "diamonds";
		break;
	case 2:
		return "clubs";
		break;
	case 3:
		return "hearts";
		break;
	case 4:
		return "spades";
		break;
	default:
		break;
	}
}

auto printArray(const array<shared_ptr<Card>, SIZE>& arr) {
	for (size_t i = 0; i < arr.size(); i++)
	{
		std::cout << "Card on position: " << i << endl;
		std::cout << "Color: " << getColor(arr[i]->color) << endl;
		std::cout << "Number: " << getNumber(arr[i]->number) << endl;
	}
}

auto createCard(Color color, Number number) {

	auto newCard = make_shared<Card>();

	if (!newCard) {
		std::cerr << "problem creating shared pointer!" << std::endl;
		exit(0);
	}

	newCard->color = color;
	newCard->number = number;

	return newCard;
}

auto fillArray(array<shared_ptr<Card>, SIZE>& arr) {
	int i = 0;
	for (size_t c = 1; c < 5; c++)
	{
		for (size_t n = 7; n < 15; n++)
		{
			auto card = createCard((Color)c, (Number)n);
			arr[i] = card;
			i++;
		}
	}
}

auto shuffleArray(array<shared_ptr<Card>, SIZE>& arr) {
	std::srand((unsigned)time(nullptr));
	for (size_t i = 0; i < arr.size(); i++)
	{
		int randomNr = (rand() % SIZE);
		shared_ptr<Card> temp = arr[randomNr];
		arr[randomNr] = arr[i];
		arr[i] = temp;
	}
}

int main(int argc, char* argv[]) {

	array<shared_ptr<Card>, SIZE> cards;

	fillArray(cards);
	printArray(cards);
	shuffleArray(cards);
	cout << "========== After shuffle ===========" << endl << endl << endl;
	printArray(cards);

	return EXIT_SUCCESS;
}