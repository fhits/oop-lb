#pragma once
#include<vector>
#include<string>
#include <fstream>
#include <iomanip>
#include <iostream>
#include "person.h"

#define print(msg) std::cout << msg << std::endl;
#define error(msg) std::cerr << msg << std::endl;

class Account {
    Person person;

    struct Transaction {
        double amount;
        std::string reason;
    };

    double currentAmount = 0;
    std::vector<Transaction> transactions;

private:
    Transaction createTransaction(double amount, std::string reason);

    void openAccount(Person& person, double amount);

public:
    Account() {};

    Account(Person& person, double amount) { openAccount(person, amount); }

    void deposit(double amount, std::string reason);

    void withdraw(double amount, std::string reason);

    void saveAccountInfo(std::string fileName);

    std::vector<std::string> readAccountInfo(std::string fileName);

    void storeAccountInfo(std::vector<std::string>& accountInfo);

    void printAccountStatement();

    void setCurrentAmount(double amount) { this->currentAmount = amount; }
    double getCurrentAmount() const { return this->currentAmount; }
    void setPerson(Person& person) { this->person = person; }
    Person getPerson() const { return this->person; }
};