#include <cassert>
#include <vector>

#include "account.h"

#define print(msg) std::cout << msg << std::endl;
#define error(msg) std::cerr << msg << std::endl;

struct Bank {
  std::vector<Account> accounts;
};

int main() {
  /* TODO: disable usage of default-constructor */

  /* always think of memory allocated - is this class object big on storage or not? */
  /* if yes , create object on heap otherwise make reference */

  Bank bank;
  // How to avoid instantiating a non-valid object with reference
  Person p1 = Person("Hans", "Gruber");
  Person person("Andreas", "Murk");
  Person person2;
  /* p1.setFirstname("Hans"); */
  /* p1.setSurname("Gruber"); */

  //	std::cout << p1.getFirstname() << " " << p1.getSurname() << std::endl;
  // assert("Hans" == p1.getFirstname());
  ////assert("Gruber" == p1.getSurname());

  Account a1 = Account(p1, 1000.123);
  /* a1.setAmount(1000.123); */
  std::cout << a1.getCurrentAmount() << std::endl;

  assert(1000.123 == a1.getCurrentAmount());

  a1.setPerson(p1);
  assert("Hans" == a1.getPerson().getFirstname());

  Account a2 = Account(p1, 546.48);
  assert(a2.getPerson().getSurname() == p1.getSurname());
  assert(a2.getCurrentAmount() == 546.48);
  a2.deposit(50.2, "fixing pc");
  assert(a2.getCurrentAmount() == 546.48 + 50.2);
  a2.deposit(-234, "i dont know what im doing here");
  // should be same value because deposit of negative value is not allowed
  assert(a2.getCurrentAmount() == 546.48 + 50.2);

  a2.withdraw(50, "new shoes");
  assert(a2.getCurrentAmount() == (546.48 + 50.20) - 50);
  // std::cout << a2.getCurrentAmount() << std::endl;

  a2.withdraw(-50, "Beatles concert");
  //  std::cout << a2.getCurrentAmount() << std::endl;

  a2.withdraw(20, "casino");
  a2.withdraw(100, "casino");
  a2.withdraw(30, "casino");
  a2.withdraw(100, "casino");
  a2.withdraw(2, "casino");
  a2.withdraw(15, "casino");
  a2.withdraw(10, "casino");
  a2.withdraw(40, "casino");
  a2.withdraw(30, "casino");

  //   std::cout << a2.getPerson().getFirstname() << " " <<
  //   a2.getPerson().getSurname() << " " << a2.getCurrentAmount() << std::endl;

  a2.saveAccountInfo("a2Info.txt");

  Account a3 = Account();
  // eventually return instead of exit()?
  std::vector<std::string> accountInfo = a3.readAccountInfo("a2Info.txt");

  assert(accountInfo[0] == a2.getPerson().getFirstname());
  assert(accountInfo[1] == a2.getPerson().getSurname());
  assert(std::abs(std::stod(accountInfo[2]) - a2.getCurrentAmount()) <
         0.000001);

  // Account a4(Person("Andreas", "Murk"), -300.00);
  /*Person p4 = Person("Andreas", "Murk");*/

  /* p4.setFirstname("Andreas"); */
  /* p4.setSurname("Murk"); */

  // assert(a4.getCurrentAmount() == 0);

  // std::vector<std::string> accountInfo2 = a3.loadAccountInfo("a3Info.txt");

  // better solution
  // std::vector<std::string> accountInfo(a3.loadAccountInfo("a2Info.txt"));

  bank.accounts.push_back(a2);

  a3.storeAccountInfo(accountInfo);
  assert(a2.getPerson().getFirstname() == a3.getPerson().getFirstname());
  assert(a2.getPerson().getSurname() == a3.getPerson().getSurname());
  assert(std::abs(a3.getCurrentAmount() - a2.getCurrentAmount()) < 0.000001);

  //    a3.storeAccountInfo(accountInfo2);

  bank.accounts.push_back(a3);

  std::cout << a3.getPerson().getFirstname() << " "
            << a3.getPerson().getSurname() << " " << a3.getCurrentAmount()
            << std::endl;

  Account testAccount;
  // result in error
  testAccount.printAccountStatement();
  
  Person anotherPerson = Person("Another", "person");
  Account newAccount = Account(anotherPerson, 5000.123);

  for (size_t i = 0; i < 100; i++) {
    if (i % 2 == 0) {
    newAccount.withdraw((double)i, "some withdraw reason");
    } else {
    newAccount.deposit((double)i, "some deposit reason");
    }
  }

  bank.accounts.push_back(newAccount);

  for (Account account : bank.accounts) {
    account.printAccountStatement();
  }

  return EXIT_SUCCESS;
}
