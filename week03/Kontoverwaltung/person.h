#pragma once

class Person {

    std::string firstname;
    std::string surname;

public:
    /* generated default constructor */
    Person() = default;

    Person(std::string firstname, std::string surname)
        : firstname(firstname), surname(surname) {}

    void setFirstname(std::string firstname) { this->firstname = firstname; }
    void setSurname(std::string surname) { this->surname = surname; }
    std::string getFirstname() const { return this->firstname; }
    std::string getSurname() const { return this->surname; }
    std::string getName() const { return this->firstname + " " + this->surname; }
};
