#include "account.h"
#include <fstream>
#include <ctime>


Account::Transaction Account::createTransaction(double amount, std::string reason) {
	return Transaction{ amount, reason };
}

void Account::openAccount(Person& person, double amount) {
	if (amount <= 0) {
		error("Cannot open account with negative amount!");
		return;
	}

	if (person.getFirstname().empty()) {
		error("Cannot open account with nameless person!");
		return;
	}

	this->status = Account::open;
	this->setPerson(person);

	this->setCurrentAmount(amount);
	this->transactions.push_back(createTransaction(amount, "current amount"));
}

void Account::closeAccount() {
	this->status = Account::closed;
	this->currentAmount = 0;
}

void Account::deposit(double amount, std::string reason) {

	if (amount <= 0) {
		print("amount cannot be negative! Cannot deposit money!");
		return;
	}
	if (this->status == Account::Status::closed) {
		std::cout << "Status of the account is: " << this->status << std::endl;
	}

	this->currentAmount += amount;

	Account::Transaction newTransaction = createTransaction(amount, reason);

	this->transactions.push_back(newTransaction);

	this->stream << *this;
}

void Account::withdraw(double amount, std::string reason) {
	if (amount <= 0) {
		print("Cannot withdraw negative amount!");
		return;
	}

	if (this->getCurrentAmount() <= amount) {
		print("Cannot withdraw money because not enough money on account!");
		return;
	}

	if (this->status == Account::Status::closed) {
		std::cout << "Status of the account is: " << this->status << std::endl;
	}

	this->currentAmount -= amount;
	this->transactions.push_back(createTransaction(-amount, reason));

	this->stream << *this;
}

void Account::saveAccountInfo(std::string fileName) {
	std::ofstream filePtr;
	filePtr.open(fileName);

	filePtr << this->getPerson().getFirstname() << "\n";
	filePtr << this->getPerson().getSurname() << "\n";
	filePtr << this->getCurrentAmount() << "\n";

	filePtr.close();
}

std::vector<std::string> Account::readAccountInfo(std::string fileName) {

	std::vector<std::string> lines;
	std::ifstream filePtr(fileName);
	if (!filePtr) {
		error("Error: wrong file name provided or allocation failed!");
		exit(0);
	}

	if (filePtr.is_open()) {
		std::string line;
		while (std::getline(filePtr, line, '\n')) {
			lines.push_back(line);
		}
		filePtr.close();
	}
	return lines;
}

void Account::storeAccountInfo(std::vector<std::string>& accountInfo) {
	Person newPerson(accountInfo[0], accountInfo[1]);
	Account::openAccount(newPerson, std::stod(accountInfo[2]));
}

void Account::printAccountStatement() {
	int i = 0;
	double sum = 0;

	if (this->transactions.empty()) {
		error("No transactions found!");
		return;
	}

	std::cout << "\n\n\n";
	std::cout << std::setfill('*') << std::setw(15);
	std::cout << ""
		<< "Account " << this->getID() << " Statement: " << this->getPerson().getName();
	std::cout << std::setw(15) << "" << std::endl;
	std::cout << "\n";

	for (Transaction transaction : this->transactions) {

		sum += transaction.amount;
		std::cout << std::setfill('.');
		std::cout << transaction.reason
			<< std::setw(60 - transaction.reason.length()) << std::right
			<< transaction.amount << std::endl;
		if (i != 0 && i % 10 == 0) {
			std::cout << "" << std::setfill('_') << std::setw(60) << ""
				<< std::endl;
			std::cout << std::setfill(' ');
			std::cout << std::left << "Sum" << std::setw(60 - 3) << std::right
				<< sum << std::endl;
			print("\n");
		}
		i++;
	}
	std::cout << "" << std::setfill('_') << std::setw(60) << "" << std::endl;
	std::cout << std::setfill(' ');
	std::cout << "Sum" << std::setw(60 - 3) << std::right << sum << std::endl;
}

std::ostream& operator<<(std::ostream& stream, const Account& account) {

	time_t curr_time;
	curr_time = time(NULL);

	tm* tm_local = localtime(&curr_time);

	Account::Transaction transaction;
	if (!account.transactions.empty()) transaction = account.transactions.back();

	stream << (tm_local->tm_mday) << "." << (tm_local->tm_mon) + 1 << "."
		<< (tm_local->tm_year) + 1900 << " - " << (tm_local->tm_hour) << ":"
		<< (tm_local->tm_min) << " " << account.getID() << " "
		<< transaction.reason << std::endl;
	return stream;
}

void Account::transferMoney(Account& receiver, double amount, std::string reason) {
	this->withdraw(amount, reason);
	receiver.deposit(amount, reason);
}
