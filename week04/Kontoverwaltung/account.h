#ifndef ACCOUNT_H
#define ACCOUNT_H

#pragma once
#include "person.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <ostream>
#include <vector>

#define print(msg) std::cout << msg << std::endl;
#define error(msg) std::cerr << msg << std::endl;

class Account {
  Person person;

  struct Transaction {
    double amount;
    std::string reason;
  };

  double currentAmount{0};
  int id{rand() % 10000};

  std::ostream &stream{std::cout};

  enum Status { open, closed };
  Status status{closed};

  std::vector<Transaction> transactions;

private:
  Transaction createTransaction(double amount, std::string reason);

  void openAccount(Person &person, double amount);

public:
  Account() = default;

  Account(Person &person, double amount)
      : person{person}, currentAmount{amount} {
    openAccount(person, amount);
  }

  Account(Person &person, double amount, std::ostream &stream)
      : person{person}, currentAmount{amount}, stream{stream} {
    openAccount(person, amount);
  }

  Account(const Account &copy)
      : person{copy.person}, currentAmount{0}, status{Status::open} {}

  ~Account() {
    /* TODO: do not create transaction for this */
    this->transactions.push_back(createTransaction(0, "deleted account"));
    this->stream << *this;
  }

  void deposit(double amount, std::string reason);

  void withdraw(double amount, std::string reason);

  void saveAccountInfo(std::string fileName);

  static std::vector<std::string> readAccountInfo(std::string fileName);

  void storeAccountInfo(std::vector<std::string> &accountInfo);

  void printAccountStatement();

  void closeAccount();

  void transferMoney(Account &receiver, double amount, std::string reason);

  void setCurrentAmount(double amount) { this->currentAmount = amount; }

  double getCurrentAmount() const { return this->currentAmount; }

  void setPerson(Person &person) { this->person = person; }

  Person getPerson() const { return this->person; }

  int getID() const { return this->id; }

  Status getStatus() const { return this->status; }

  friend std::ostream &operator<<(std::ostream &stream, const Account &account);
};

#endif /* ACCOUNT_H */
