#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include "account.h"

#define print(msg) std::cout << msg << std::endl;
#define error(msg) std::cerr << msg << std::endl;

#define LOGGING_FILENAME "logging.txt"
#define LOGGING_DESTROYED_FILENAME "destroyed.txt"

struct Bank {
	std::vector<Account> accounts;
	int length = {0};
};

void test_person_creation();
void test_stream_definition();
void test_account_creation();
void test_deposit_amount();
void test_withdraw_amount();
void test_save_account_info();
void test_save_account_info();
void test_copy_account();
void test_bank_accounts();
void test_print_account_statement();

int main() {

	std::srand(time(NULL));

	test_stream_definition();
	test_stream_definition();
	test_account_creation();
	test_deposit_amount();
	test_withdraw_amount();
	test_save_account_info();

	return EXIT_SUCCESS;
}

void test_stream_definition() {

	std::ofstream filePtr(LOGGING_FILENAME, std::ios_base::app);
	Person person("Test", "Name");
	Account testAccount(person, 1000, std::cout);
	testAccount.deposit(100, "new printer");

	filePtr.close();
}

void test_person_creation() {

	Person p1("Hans", "Gruber");

	std::cout << p1.getFirstname() << " " << p1.getSurname() << std::endl;
	assert("Hans" == p1.getFirstname());
	assert("Gruber" == p1.getSurname());
}

void test_account_creation() {

	Person p1("Hans", "Murk");
	Account a1 = Account(p1, 1000.123);
	a1.setCurrentAmount(1000.123);
	std::cout << a1.getCurrentAmount() << std::endl;

	assert(1000.123 == a1.getCurrentAmount());

	a1.setPerson(p1);
	assert("Hans" == a1.getPerson().getFirstname());
}

void test_deposit_amount() {
	Person p1("Andreas", "Murk");
	Account a2 = Account(p1, 546.48);
	assert(a2.getPerson().getSurname() == p1.getSurname());
	assert(a2.getCurrentAmount() == 546.48);
	a2.deposit(50.2, "fixing pc");
	assert(a2.getCurrentAmount() == 546.48 + 50.2);
	a2.deposit(-234, "i dont know what im doing here");

	// should be same value because deposit of negative value is not allowed
	assert(a2.getCurrentAmount() == 546.48 + 50.2);
}

void test_withdraw_amount() {
	double amount = 546.48;
	Person p1("Andreas", "Murk");
	Account a2 = Account(p1, amount);
	a2.deposit(50.20, "new shoes");
	a2.withdraw(50, "Beatles concert");

	assert(a2.getCurrentAmount() == (amount + 50.20) - 50);
}

void test_negative_withdraw_amount() {
	Person p1("Andreas", "Murk");
	Account a2 = Account(p1, 19999.123123);
	a2.deposit(50, "new shoes");
	a2.withdraw(50, "Beatles concert");
	assert(a2.getCurrentAmount() == (546.48 + 50.20) - 50);
	// std::cout << a2.getCurrentAmount() << std::endl;

	a2.withdraw(50, "Beatles concert");
	//  std::cout << a2.getCurrentAmount() << std::endl;

	a2.withdraw(20, "casino");
	a2.withdraw(100, "casino");
	a2.withdraw(30, "casino");
	a2.withdraw(100, "casino");
	a2.withdraw(2, "casino");
	a2.withdraw(15, "casino");
	a2.withdraw(10, "casino");
	a2.withdraw(40, "casino");
	a2.withdraw(30, "casino");
}

void test_save_account_info() {
	Person p1("Andreas", "Murk");
	Account a2 = Account(p1, 195.123);
	a2.withdraw(50, "new shoes");
	a2.withdraw(20, "wife");
	a2.withdraw(100, "wife");
	a2.withdraw(30, "wife");
	a2.withdraw(100, "wife");
	a2.withdraw(2, "wife");
	a2.withdraw(15, "wife");
	a2.withdraw(10, "wife");
	a2.withdraw(40, "wife");
	a2.withdraw(30, "wife");

	a2.saveAccountInfo("a2Info.txt");

	std::vector<std::string> accountInfo = Account::readAccountInfo("a2Info.txt");

	assert(accountInfo[0] == a2.getPerson().getFirstname());
	assert(accountInfo[1] == a2.getPerson().getSurname());
	assert(std::abs(std::stod(accountInfo[2]) - a2.getCurrentAmount()) <
		0.000001);
}

void test_store_account_info() {
	Person p4 = Person("Andreas", "Murk");
	Account a4(p4, -300.00);

	p4.setFirstname("Andreas");
	p4.setSurname("Murk");

	a4.saveAccountInfo("a4Info.txt");
	std::vector<std::string> accountInfo = a4.readAccountInfo("a2Info.txt");
	Account a3{};
	a3.storeAccountInfo(accountInfo);
	assert(a4.getPerson().getFirstname() == a3.getPerson().getFirstname());
	assert(a4.getPerson().getSurname() == a3.getPerson().getSurname());
	assert(std::abs(a3.getCurrentAmount() - a4.getCurrentAmount()) < 0.000001);
}

void test_copy_account() {
	Person anotherPerson = Person("Another", "person");
	Account newAccount = Account(anotherPerson, 5000.123);

	Account copyAccount = newAccount;
	copyAccount.setCurrentAmount(123.0);

	Account anotherAccount = copyAccount;

	assert(copyAccount.getCurrentAmount() == 0);
	assert(newAccount.getCurrentAmount() != 0);
	assert(newAccount.getPerson().getFirstname() ==
		copyAccount.getPerson().getFirstname());
}

void test_print_account_statement() {
	Person p1("Another", "testperson");
	Account testAccount(p1, 1293);
	for (size_t i = 0; i < 100; i++) {
		if (i % 2 == 0) {
			testAccount.withdraw((double)i, "some withdraw reason");
		}
		else {
			testAccount.deposit((double)i, "some deposit reason");
		}
	}
	testAccount.printAccountStatement();
}

void test_bank_accounts() {
	Bank bank;

	Person person1("Andreas", "Murk");
	Person person2("Daniel", "Murk");
	Person person3("Daniel", "Ertl");

	Account a1(person1, 1234);
	Account a2(person2, 4034);
	Account a3(person3, 9239);

	bank.accounts.push_back(a1);

	bank.accounts.push_back(a2);

	bank.accounts.push_back(a3);

	for (Account account : bank.accounts) {
		account.printAccountStatement();
	}
}

void test_account_status() {
	Person statusPerson = Person("Status", "Test");
	Account statusAccount = Account(statusPerson, 2000);
	statusAccount.closeAccount();
	statusAccount.deposit(2.3, "testStatus");
}

void test_transfer_money() {
	Person person("Daniel", "Ertl");

	Account newAccount(person, 9239);

	Account copyAccount = newAccount;

	newAccount.transferMoney(copyAccount, 50.0, "Transfer money to copyAccount");
	newAccount.printAccountStatement();
	copyAccount.printAccountStatement();
}
