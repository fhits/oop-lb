#include "Buffer.h"
#include <cstddef>
#include <iostream>

#define error(msg)                                                             \
  {                                                                            \
    std::cout << msg << std::endl;                                             \
    exit(EXIT_FAILURE);                                                        \
  }

bool Buffer::isValidBufferSize() {

  if (this->buffer_Size <= 0) {
    printf("Buffer size is not valid!\n");
    return false;
  }
  return true;
}

bool Buffer::isValid(const char *text) {

  if (this->buffer_Size < strlen(text) + 1) {
    printf("Buffer size is too small!\n");
    return false;
  }

  if (!this->buffer_Data || !text) {
    printf("No valid Buffer given!\n");
    return false;
  }

  return true;
}

bool Buffer::isValid(const char* text, const int currentPosition) {

    if (this->buffer_Size < currentPosition + strlen(text)) {
        //printf("Buffer size is too small!\n");
        return false;
    }

    if (!this->buffer_Data || !text) {
        //printf("No valid Buffer given!\n");
        return false;
    }

    return true;
}

void Buffer::fillBuffer(const char *bytes) {
  size_t byteSize = strlen(bytes);

  if (byteSize < 2) {
    printf("Must provide more than one byte!\n");
    return;
  }

  if (this->isValidBufferSize()) {
      for (size_t i = 0; this->isValid(bytes, i); i += byteSize) {
        memcpy(this->buffer_Data + i, bytes, byteSize);
      }
  }
}

// TODO:: look for method to find first \0 sequence if buffer is not empty
void Buffer::fillBuffer(const char byte) {
  for (size_t i = 0; i < (this->buffer_Size); i++) {
   memcpy(this->buffer_Data + i, &byte, 1);
  }
}

void Buffer::writeBuffer(const char *text) {
  size_t textSize = strlen(text) + 1;

  if (this->isValid(text) && this->isValidBufferSize()) {

    memcpy(this->buffer_Data + this->buffer_Pos, text, textSize);

    this->buffer_Pos += textSize;
  }
}

void Buffer::printBufferData() {

  if (this->isValidBufferSize()) {
    for (size_t i = 0; i < this->buffer_Size; ++i) {
      std::cout << this->getBufferData()[i];
    }
    std::cout << std::endl;
  }
}
