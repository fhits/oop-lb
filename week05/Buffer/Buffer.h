#ifndef BUFFER_H
#define BUFFER_H

#include "Identifier.h"
#include <cstddef>
#include <cstdio>
#include <iostream>
#include <string.h>

class Buffer {

private:
  unsigned int id{0};
  char *buffer_Data{nullptr};
  size_t buffer_Size{0};
  size_t buffer_Pos{0};

public:

  Buffer() = default;

  Buffer(size_t size) : buffer_Size{size} {
    this->buffer_Data = new char[buffer_Size];
    this->id = Identifier::getInstance().generateID();
    printf("%i: created buffer\n", this->id);
  }

  Buffer(const char *text) : Buffer(strlen(text) + 1) {
    this->writeBuffer(text);
  }

  Buffer(const Buffer &other) {
    this->buffer_Size = other.buffer_Size;
    this->buffer_Data = new char[this->buffer_Size];
    this->id = Identifier::getInstance().generateID();
    memcpy(this->buffer_Data, other.buffer_Data, this->buffer_Size);
    printf("%i: copied Buffer!\n", this->id);
  }

  Buffer(Buffer &&other) noexcept {
    this->buffer_Size = other.buffer_Size;
    this->buffer_Data = other.buffer_Data;
    this->id = other.id;

    other.buffer_Size = 0;
    other.buffer_Data = nullptr;
    Identifier::getInstance().generateID();
    printf("%i: moved Buffer!\n", this->id);
  }

  ~Buffer() {
    printf("%i: destroyed Buffer!\n", this->id);
    delete[] buffer_Data;
  }

  Buffer& operator=(Buffer&& other) noexcept {
      if(this != &other){
          delete[] this->buffer_Data;
          this->buffer_Size = other.buffer_Size;
          this->buffer_Data = other.buffer_Data;
          this->id = other.id;

          other.buffer_Size = 0;
          other.buffer_Data = nullptr;
          Identifier::getInstance().generateID();
          printf("%i: moved Buffer with = operator!\n", this->id);
          }
      return *this;
  }

  void fillBuffer(const char *bytes);

  void fillBuffer(const char byte);

  const unsigned int getID() { return this->id; }

  void setID(const unsigned int ID) { this->id = ID; }

  const size_t getBufferSize() { return this->buffer_Size; }

  const char *getBufferData() { return this->buffer_Data; }

  void writeBuffer(const char *text);

  void printBufferData();

  bool isValidBufferSize();

  bool isValid(const char *text);

  bool isValid(const char* text, const int currentPosition);
};
#endif /* ifndef BUFFER_H */
