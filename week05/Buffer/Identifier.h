#ifndef IDENTIFIER_H
#define IDENTIFIER_H

class Identifier {
public:
  static Identifier getInstance() { return s_Identifier; }

  const unsigned int generateID() { return s_Identifier.id++; }

  /* Identifier(const Identifier &other) = delete; */

  /* Identifier(const Identifier &&other) = delete; */

private:
  static Identifier s_Identifier;

  unsigned int id{0};

  Identifier() = default;
};

#endif /* ifndef IDENTIFIER_H */
