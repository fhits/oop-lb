#include "Buffer.h"
#include "Identifier.h"
#include <array>
#include <cstdio>
#include <iostream>
#include <vector>

#define test(check, msg)                                                       \
  if (check) {                                                                 \
    std::cerr << "TEST FAILED: " << msg << std::endl;                          \
    exit(EXIT_FAILURE);                                                        \
  }

#define string_equals(string1, string2, msg)                                   \
  test((strcmp(string1, string2) != 0), msg)

#define print(msg)                                                             \
  {                                                                            \
    std::cout << "-------------------------------" << std::endl;               \
    std::cout << msg << std::endl;                                             \
    std::cout << "-------------------------------" << std::endl;               \
  }

void test_buffer_creation();
void test_buffer_write();
void test_buffer_write_multiple();
void test_buffer_copy_data();
void test_create_buffer();
void test_use_buffer();
void test_create_identifier();
void test_create_buffers_vector();
void test_create_buffers_array();
void test_create_buffer_array_for();
void test_fill_buffer_single();
void test_fill_buffer_multiple();
void test_deep_copy();
void test_overloaded_operator();
void test_move_constructor();

/* if (Buffer& v) then the cctor would not be called because the reference is
 * transferred */
void useBuffer(Buffer v) {}

Buffer createBuffer() {
  Buffer buffer = Buffer("createdBuffer");
  return buffer;
}

int main(int argc, char *argv[]) {

   test_buffer_creation(); 
   test_buffer_copy_data(); 
   test_use_buffer(); 
   test_create_buffer(); 
   test_create_identifier(); 
   test_buffer_write(); 
   test_buffer_write_multiple();
   test_fill_buffer_multiple(); 
   test_fill_buffer_single(); 
   test_deep_copy(); 
   test_create_buffers_array();
   test_create_buffer_array_for();
   test_create_buffers_vector();
   test_overloaded_operator();
   test_move_constructor();

  return 0;
}

void test_buffer_creation() {
  print("Test Buffer creation: ");
  const char *text = "Hello there!\0";
  Buffer buffer = Buffer(text);

  buffer.printBufferData();

  string_equals(buffer.getBufferData(), text, "Strings should be equal!");
  test(!(buffer.getBufferSize() == strlen(text) + 1),
       "Buffer size does not match!");
}

void test_buffer_write() {
  print("Test Buffer write: ");

  Buffer buffer = Buffer(100);

  buffer.writeBuffer("Test");

  buffer.printBufferData();

  string_equals(buffer.getBufferData(), "Test",
                "method writeBuffer did not work properly!")
}

void test_buffer_write_multiple() {
  print("Test Buffer multiple write: ");

  Buffer buffer = Buffer(100);

  const char *text1 = "Some text in here!";
  const char *text2 = "App";

  buffer.writeBuffer(text1);
  buffer.writeBuffer(text2);

  string_equals(buffer.getBufferData(), "Some text in here!\0App",
                "setBuffer did not work properly!")
}

void test_buffer_copy_data() {
  print("Test copy data: ");
  Buffer newBuffer = Buffer(100);
  newBuffer.writeBuffer("Hi there!");
  Buffer copiedBuffer = newBuffer;

  string_equals(newBuffer.getBufferData(), copiedBuffer.getBufferData(),
                "values are not copied correctly!");
}

void test_create_buffer() {
  print("Test creatBuffer function: ");

  // calls mctor because createBuffer() returns rvalue
  Buffer create = createBuffer();
  string_equals(create.getBufferData(), "createdBuffer",
                "created Buffer is not equal!");
}

void test_use_buffer() {
  print("Test Buffer creation: ");
  Buffer use = Buffer("useBuffer");
  // calls cctor because "use" variable is an lvalue
  useBuffer(use);
}

void test_create_identifier() {
  print("Test cration of Identifier: ");
  Buffer buffer = Buffer(100);
  Buffer buffer2 = Buffer(100);
  

  // must be same as buffer2 holds last id
  test(Identifier::getInstance().generateID() == buffer2.getID(),
       "IDs must be unequal!");
}

void test_create_buffers_vector() {
  print("Test vector of buffer objects: ");
  std::vector<Buffer> bufferVector;

  for (int i = 0; i < 5; ++i) {
    Buffer newBuffer = Buffer(100);
    bufferVector.push_back(newBuffer);
  }
}

void test_create_buffers_array() {

    Buffer newBuffer = Buffer(100);
    Buffer newBuffer1 = Buffer(100);
    Buffer newBuffer2 = Buffer(100);
    Buffer newBuffer3 = Buffer(100);
    Buffer newBuffer4 = Buffer(100);

    std::array<Buffer, 5> bufferArray{ newBuffer, newBuffer1, newBuffer2, newBuffer3, newBuffer4 };
}

void test_create_buffer_array_for() {
    std::array<Buffer, 5> bufferArray{};
    for (int i = 0; i < 5; ++i) {
        Buffer newBuffer = Buffer(100);
        bufferArray[i] = std::move(newBuffer);
    }
}

void test_deep_copy() {
  print("Test Deep copy") Buffer buffer = Buffer(20);
  Buffer deepBuffer = buffer;

  const char *bufferPtrAdr = buffer.getBufferData();
  const char *deepBufferPtrAdr = deepBuffer.getBufferData();

  test((&bufferPtrAdr == &deepBufferPtrAdr), "Pointers are not different!");
}

void test_fill_buffer_single() {
  print("Test Fill Buffer single");

  Buffer testBuffer = Buffer(100);

  testBuffer.fillBuffer('+');

  testBuffer.printBufferData();
}

void test_fill_buffer_multiple() {
  print("Test Fill Buffer multiple");

  Buffer buffer = Buffer(10);

  buffer.fillBuffer("*_*");

  buffer.printBufferData();
}

void test_overloaded_operator() {
    print("Test overloaded = operator");
    
    Buffer sourceBuffer = Buffer("MoveText");
    Buffer destBuffer = Buffer(100);

    destBuffer = std::move(sourceBuffer);

    string_equals(destBuffer.getBufferData(), "MoveText",
        "moved with operator = buffer data is not equal!");
    
}

void test_move_constructor() {
    print("Test move constructor");

    Buffer sourceBuffer = Buffer(20);
    sourceBuffer.writeBuffer("moveText");
    Buffer destBuffer = Buffer(std::move(sourceBuffer));

    string_equals(destBuffer.getBufferData(), "moveText",
        "moved with buffer data is not equal!");
}
