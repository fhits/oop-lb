#include "Equilateral.h"
#include <iostream>

void Equilateral::equilateral() {

  std::cout << "I am an equilateral triangle based on a isosceles triangle" << std::endl;
  this->Isosceles::isosceles();
}

std::string Equilateral::getClassName() { return "Equilateral"; }

void Equilateral::displayClassName() {

  std::cout << "I am a " << this->getClassName() << std::endl;
};
