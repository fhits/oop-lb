#ifndef EQUILATERAL
#define EQUILATERAL

#include "Isosceles.h"
#include <iostream>

class Equilateral : public Isosceles {

private:
public:
	Equilateral() {
		std::cout << "Constructor of Equilateral called" << std::endl;
	}
  Equilateral(const Equilateral &other) = delete;
  Equilateral(Equilateral &&other) = delete;

  ~Equilateral() {
	  std::cout << "Destructor of Equilateral called" << std::endl;
  }

  void equilateral();
  std::string getClassName();
  void displayClassName() override;
};
#endif /* ifndef EQUILATERAL */
