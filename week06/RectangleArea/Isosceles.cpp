#include "Isosceles.h"
#include <iostream>

void Isosceles::isosceles() {

  std::cout << "I am an isosceles triangle based on triangle" << std::endl;
  this->Triangle::triangle();
}

std::string Isosceles::getClassName() { return "Isosceles"; }

void Isosceles::displayClassName() {

  std::cout << "I am a " << this->getClassName() << std::endl;
}
