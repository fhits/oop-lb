#ifndef ISOSCELES_H
#define ISOSCELES_H

#include "Triangle.h"
#include <iostream>

class Isosceles : public Triangle {

private:
public:
  Isosceles() {
	  std::cout << "Constructor of Isosceles called" << std::endl;
	}
  Isosceles(const Isosceles &other) = delete;
  Isosceles(Isosceles &&other) = delete;

  ~Isosceles() {
	   std::cout << "Destructor of Isosceles called" << std::endl;
  }

  void isosceles();
  std::string getClassName();
  void displayClassName() override;
};
#endif /* ifndef ISOSCELES_H */
