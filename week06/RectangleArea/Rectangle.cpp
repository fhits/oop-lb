#include <iostream>

#include "Rectangle.h"

void Rectangle::display() {
	std::cout << "h=" << Rectangle::getHeight() << " w=" << Rectangle::getWidth() << std::endl;
}