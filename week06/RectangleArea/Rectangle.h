#ifndef RECTANGLE_H
#define RECTANGLE_H
class Rectangle {

private:
  double height{0};
  double width{0};

public:
  Rectangle() = default;

  Rectangle(double height, double width) : height{height}, width{width} {};

  void setHeight(const double height) { this->height = height; };

  double getHeight() const { return this->height; }

  void setWidth(const double width) { this->width = width; };

  double getWidth() const { return this->width; }

  virtual void display();
};
#endif /* ifndef RECTANGLE_H */

