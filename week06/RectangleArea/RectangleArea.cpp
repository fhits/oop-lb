#include "RectangleArea.h"
#include <cstdint>
#include <limits>

#define error(msg)                                                             \
  { std::cerr << msg << std::endl; }

void RectangleArea::readInput() {

  double height, width = 0.0;

  do {
    std::cout << "Type in width and height:" << std::endl;
    std::cin >> height;
    std::cin >> width;
    this->clearCin(std::cin);
  } while ((height < 1) || (width >= 100) || (width <= 0));

  this->setWidth(width);
  this->setHeight(height);
}

void RectangleArea::clearCin(std::istream &stream) {

  // This method is used to determine if string is typed in
  stream.clear();
  while (stream.get() != '\n') {
    continue;
  }
}

void RectangleArea::display() {
  double area = Rectangle::getHeight() * Rectangle::getWidth();
  Rectangle::display();
  std::cout << "A=" << area << std::endl;
}
