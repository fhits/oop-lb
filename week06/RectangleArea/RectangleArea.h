#ifndef RECTANGLEAREA_H
#define RECTANGLEAREA_H

#include "Rectangle.h"
#include <iostream>

class RectangleArea : public Rectangle {

private:
public:
  RectangleArea() = default;
  RectangleArea(double height, double width) : Rectangle(height, width) {}

  void readInput();
  void clearCin(std::istream &stream);
  void display() override;
};
#endif /* ifndef R */
