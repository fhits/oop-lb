#include "Triangle.h"
#include <iostream>

void Triangle::triangle() { std::cout << "I am a triangle" << std::endl; }

std::string Triangle::getClassName() { return "Triangle"; }

void Triangle::displayClassName() {

  std::cout << "I am a " << this->getClassName() << std::endl;
}

