#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <iostream>
#include <string>

class Triangle {

private:
public:

	Triangle() {
		std::cout << "Constructor of Triangle called" << std::endl;
	}
  Triangle(const Triangle& other) = delete;
  Triangle(Triangle&& other) = delete;

  ~Triangle() {
	  std::cout << "Destructor of Equilateral called" << std::endl;
  }

  void triangle();
  std::string getClassName();
  virtual void displayClassName();
};
#endif /* ifndef TRIANGLE_H */

