#include <iostream>
#include <sstream>

#include "Rectangle.h"
#include "RectangleArea.h"
#include "Triangle.h"
#include "Isosceles.h"
#include "Equilateral.h"

#define test(check, msg)                                                       \
  if (!(check)) {                                                              \
    std::cerr << "TEST FAILED: " << msg << std::endl;                          \
    exit(EXIT_FAILURE);                                                        \
  }

#define string_equals(string1, string2, msg)                                   \
  test((strcmp(string1, string2) != 0), msg)

#define print(msg)                                                             \
  {                                                                            \
    std::cout << "-------------------------------" << std::endl;               \
    std::cout << msg << std::endl;                                             \
    std::cout << "-------------------------------" << std::endl;               \
  }

void testRectangleAccessor();
void testRectangleDisplay();
void testRectangleAreaReadInput();
void testRectangleAreaDisplay();
void testRoutine();
void testMultipleInheritance();
void testConDestructorMultiple();

int main(int argc, char *argv[]) {
  testRectangleAreaReadInput();
  
  testRectangleAreaDisplay();

  testRoutine();

  testMultipleInheritance();

  testConDestructorMultiple();

  return 0;
}

void testRectangleAccessor() {
  print("Rectangle test");

  Rectangle rectangle = Rectangle(5.7, 7.8);

  test(rectangle.getHeight() == 5.7, "The rectangle heigth should be equal!");
  test(rectangle.getWidth() == 7.8, "The rectangle width should be equal!");
}

void testRectangleDisplay() {
  print("Rectangle display test");

  Rectangle rectangle = Rectangle(5.7, 7.8);
  rectangle.display();
}

void testRectangleAreaReadInput() {

    print("Rectangle read input test:");

  RectangleArea r = RectangleArea();

  r.readInput();

  r.display();
}

void testRectangleAreaDisplay() {
    print("RectangleArea display test:");
  RectangleArea r = RectangleArea(4, 2);
  r.display();
}

void testRoutine() {
    print("Starting test routine:");

  /* Declare a RectangleArea object */
  RectangleArea r_area;

  /* Read width and height */
  r_area.readInput();

  /* Read width and height */
  r_area.Rectangle::display();

  /* Print the area */
  r_area.display();
}

void testMultipleInheritance() {
    print("Multiple inheritance test:");

  Equilateral eq;

  eq.equilateral();
}

void testConDestructorMultiple() {
    print("Test Con and Destructor:")
    Equilateral* eq = new Equilateral();
    delete eq;
}

