#include "GeoEllipse.h"

class GeoCircle : public GeoEllipse {

public:
  GeoCircle() = delete;
  GeoCircle(double r) : GeoEllipse(r, r) {}

  std::string getClassName() override;
};
