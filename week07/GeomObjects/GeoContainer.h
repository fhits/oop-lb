#ifndef GEOCONTAINER_H
#define GEOCONTAINER_H

#include "IGeomObj.h"
#include <memory>
#include <vector>

class GeoContainer : virtual public IGeomObj {
protected:
  std::vector<std::shared_ptr<IGeomObj>> container;
  void area() override;
  void circumference() override;

public:
  GeoContainer() = default;

  void pushObject(std::shared_ptr<IGeomObj> object) {
    this->container.push_back(object);
  }

  void output() override;
  std::string getClassName() override { return "GeoContainer"; };

};
#endif /* ifndef GEOCONTAINER_H */
