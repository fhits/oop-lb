#include "GeoEllipse.h"

void GeoEllipse::output() {
  std::cout << "Geometric object > " << this->getClassName()
            << " > a: " << this->getA() << " b: " << this->getB()
            << " area: " << this->getAr()
            << " circumference: " << this->getCir() << std::endl;
}

void GeoEllipse::area() { this->setAr(this->getA() * this->getB() * M_PI); }

void GeoEllipse::circumference() {
  this->setCir(M_PI * (1.5 * (this->getA() + this->getB()) -
                       std::sqrt(this->getA() * this->getB())));
}

std::string GeoEllipse::getClassName() { return "GeoEllipse"; }
