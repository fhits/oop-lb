#ifndef GEOELLIPSE_H
#define GEOELLIPSE_H

#include "IGeomObj.h"
#include <cmath>
#include <iostream>

# define M_PI 3.14159265358979323846

class GeoEllipse : virtual public IGeomObj {

protected:
  double a = 0;
  double b = 0;

  void area() override;
  void circumference() override;

public:
  GeoEllipse() = delete;
  GeoEllipse(double a, double b) : a(a), b(b) {
      std::cout << "Constructor " << this->getClassName() << std::endl;
    area();
    circumference();
  }
  GeoEllipse(const GeoEllipse& ellipse): a(ellipse.a), b(ellipse.b){
      std::cout << "Copy Constructor " << this->getClassName() << std::endl;
  }

  virtual ~GeoEllipse() {
      std::cout << "Destructor " << this->getClassName() << std::endl;
  }

  const double getA() const { return this->a; }
  void setA(double const a) { this->a = a; }
  const double getB() const { return this->b; }
  void setB(double const b) { this->b = b; }
  void output() override;
  std::string getClassName() override;
};

#endif /* ifndef GEOELLIPSE_H */
