#include "GeoHouse.h"

void GeoHouse::output() {
  std::cout << "Geometric object > " << this->getClassName()
            << " area: " << this->getAr()
            << " circumference: " << this->getCir() << std::endl;
};

void GeoHouse::area() {
  double areaSum = 0;
  for (auto obj : this->container) {
    areaSum += obj->getAr();
  }
  this->setAr(areaSum);
};

void GeoHouse::circumference() {
  double cirSum = 0, rectWidth = 0;
  for (auto obj : this->container) {
		cirSum += obj->getCir();
    if (obj->getClassName() == "GeoRect") {
			/* TODO: use smart pointer */
      GeoRect *gr = dynamic_cast<GeoRect *>(obj.get());
      if(gr) rectWidth = 2 * gr->getWidth();
    }
  }
  this->setCir(cirSum - rectWidth);
};
