#ifndef GEOHOUSE_H
#define GEOHOUSE_H

#include "GeoContainer.h"
#include "GeoRect.h"
#include "GeoTriangle.h"
/* #include <memory> */

class GeoHouse : virtual public GeoContainer {

private:
  void circumference() override;
  void area() override;

public:
  void output() override;
  std::string getClassName() override { return "GeoHouse"; };
  GeoHouse(std::shared_ptr<GeoTriangle> triangle,
           std::shared_ptr<GeoRect> rectangle) {
    this->pushObject(triangle);
    this->pushObject(rectangle);

    this->area();
    this->circumference();
  }

  ~GeoHouse() {
    std::cout << "Destructor " << this->getClassName() << std::endl;
  }
};

#endif /* ifndef GEOHOUSE_H */
