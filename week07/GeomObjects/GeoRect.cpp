#include "GeoRect.h"

void GeoRect::output() {
  std::cout << "Geometric object > " << this->getClassName()
            << " > width: " << this->getWidth()
            << " height: " << this->getHeight() << " area: " << this->getAr()
            << " circumference: " << this->getCir() << std::endl;
}

void GeoRect::area() { this->setAr(this->width * this->height); }

void GeoRect::circumference() {
  this->setCir(2 * this->getWidth() + 2 * this->getHeight());
}

std::string GeoRect::getClassName() { return "GeoRect"; }
