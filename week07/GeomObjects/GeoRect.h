#ifndef GEORECT_H
#define GEORECT_H

#include "IGeomObj.h"
#include <iostream>

class GeoRect : virtual public IGeomObj {

protected:
  double width = 0;
  double height = 0;

  void area() override;
  void circumference() override;

public:
  GeoRect() = delete;
  GeoRect(double width, double heigth) : width(width), height(heigth) {
    std::cout << "Constructor " << this->getClassName() << std::endl;
    area();
    circumference();
  }

  GeoRect(const GeoRect& rect): width(rect.width), height(rect.height){
      std::cout << "Copy Constructor " << this->getClassName() << std::endl;
  }

  virtual ~GeoRect() {
      std::cout << "Destructor " << this->getClassName() << std::endl;
  }

  const double getWidth() const { return this->width; }
  void setWidth(double const width) { this->width = width; }
  const double getHeight() const { return this->height; }
  void setHeight(double const height) { this->height = height; }
  void output() override;
	std::string getClassName() override;
};

#endif /* ifndef GEOSQUARE_H */
