#include "GeoSquare.h"

#include <iostream>


void GeoSquare::output() {
  std::cout << "Geometric object > " << this->getClassName()
            << " > width: " << this->getWidth() << " area: " << this->getAr()
            << " circumference: " << this->getCir() << std::endl;
}

std::string GeoSquare::getClassName() { return "GeoSquare"; }

