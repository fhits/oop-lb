#ifndef GEOSQUARE_H
#define GEOSQUARE_H

#include "GeoRect.h"

class GeoSquare : public GeoRect {
public:
  GeoSquare() = delete;
  GeoSquare(double width) : GeoRect(width, width) {
	  std::cout << "Constructor " << this->getClassName() << std::endl;
  }

  virtual ~GeoSquare() {
	  std::cout << "Destructor " << this->getClassName() << std::endl;
  }

  void output() override;
	std::string getClassName() override;
};

#endif /* ifndef GEOSQUARE_H */
