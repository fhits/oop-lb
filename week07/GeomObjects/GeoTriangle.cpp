#include "GeoTriangle.h"

#include <iostream>

void GeoTriangle::output() {
  std::cout << "Geometric object > " << this->getClassName() << " > a: " << this->getA()
            << " b: " << this->getB() << " c: " << this->getC()
            << " area: " << this->getAr()
            << " circumference: " << this->getCir() << std::endl;
}

void GeoTriangle::area() { this->setAr(0.5 * this->getC() * this->getHeight()); }

void GeoTriangle::circumference() { this->setCir(this->getA() + this->getB() + this->getC()); }

std::string GeoTriangle::getClassName() { return "GeoTriangle"; }
