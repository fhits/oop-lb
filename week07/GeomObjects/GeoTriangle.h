#ifndef GEOTRIANGLE_H
#define GEOTRIANGLE_H

#include "IGeomObj.h"
#include<iostream>

class GeoTriangle : virtual public IGeomObj {

protected:
  /* TODO: double height */
  double a = 0, b = 0, c = 0, height = 0;

  void area() override;
  void circumference() override;

public:
  GeoTriangle() = delete;
  GeoTriangle(double a, double b, double c, double height)
      : a(a), b(b), c(c), height(height) {
      std::cout << "Constructor " << this->getClassName() << std::endl;
    area();
    circumference();
  }
  GeoTriangle(const GeoTriangle& triangle) : a(triangle.a), b(triangle.b), c(triangle.c), height(triangle.height) {
      std::cout << "Copy Constructor " << this->getClassName() << std::endl;
  }
  virtual ~GeoTriangle() {
      std::cout << "Destructor " << this->getClassName() << std::endl;
  }

  const double getA() const { return this->a; }
  void setA(double const a) { this->a = a; }
  const double getB() const { return this->b; }
  void setB(double const b) { this->b = b; }
  const double getC() const { return this->c; }
  void setC(double const c) { this->c = c; }
  const double getHeight() const { return this->height; }
  void setHeight(double const height) { this->height = height; }
  void output() override;
  std::string getClassName() override;
};

#endif /* ifndef GEOTRIANGLE_H */
