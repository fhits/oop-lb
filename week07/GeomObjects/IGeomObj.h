#ifndef IGEOMOBJ_H
#define IGEOMOBJ_H

#include "IUniqueID.h"
#include <iostream>
#include <string>

class IGeomObj : virtual public IUniqueID {
private:
  double ar = 0;
  double cir = 0;

protected:
  virtual void area() = 0;
  virtual void circumference() = 0;
  int getUniqueId() { return IUniqueID::counter++; };

  IGeomObj() {
    std::cout << "Constructor " << this->getClassName() << std::endl;
  }

  IGeomObj(const IGeomObj& object) {
	  std::cout << "Copy Constructor " << this->getClassName() << std::endl;
  }
  virtual ~IGeomObj() {
    std::cout << "Destructor " << this->getClassName() << std::endl;
  }

public:
  virtual void output() = 0;
  void setAr(const double ar) { this->ar = ar; }

  const double getAr() const { return this->ar; }

  void setCir(const double cir) { this->cir = cir; }

  const double getCir() const { return this->cir; }

  virtual std::string getClassName() { return "IGeomObj"; }
};

#endif /* ifndef IGEOMOBJ_H */
