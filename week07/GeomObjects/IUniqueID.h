#ifndef IUNIQUEID_H
#define IUNIQUEID_H

class IUniqueID {
protected:
  static int counter;

  virtual int getUniqueId() = 0;
};

#endif /* ifndef IUNIQUEID_H */
