#include "GeoCircle.h"
#include "GeoContainer.h"
#include "GeoEllipse.h"
#include "GeoHouse.h"
#include "GeoRect.h"
#include "GeoSquare.h"
#include "GeoTriangle.h"

#include <cstring>

#define test(check, msg)                                                       \
  if (!(check)) {                                                              \
    std::cerr << "TEST FAILED: " << msg << std::endl;                          \
    exit(EXIT_FAILURE);                                                        \
  }

#define string_equals(string1, string2, msg)                                   \
  test((strcmp(string1, string2) == 0), msg)

#define print(msg)                                                             \
  {                                                                            \
    std::cout << "-------------------------------" << std::endl;               \
    std::cout << msg << std::endl;                                             \
    std::cout << "-------------------------------" << std::endl;               \
  }

void testGeoRect();
void testGeoSquare();
void testGeoRectSquare();
void testGeoTriangle();
void testGeoEllipse();
void testGeoCircle();
void testGeoEllipseCircle();
void testGeoContainer();
void testUniqueID();
void testCopyConstructor();
void testGeoHouse();

int main(int argc, char *argv[]) {
   testGeoRect(); 
   testGeoSquare(); 
   testGeoRectSquare(); 
   testGeoTriangle(); 
   testGeoEllipse();
   testGeoCircle();
   testGeoEllipseCircle();
   testGeoContainer(); 
  testGeoHouse();
   testUniqueID(); 
   testCopyConstructor(); 

  return 0;
}

void testGeoRect() {
    print("Start test GeoRect:");
  GeoRect rect = GeoRect(2, 3);
  rect.output();
  string_equals(rect.getClassName().c_str(), "GeoRect", "Class name should be GeoRect!");
  test(rect.getAr() == 6 && rect.getCir() == 10, "Something with the calculation went wrong!");
}

void testGeoSquare() {
    print("Start test GeoSquare:");
  GeoSquare squ = GeoSquare(4);
  squ.output();
  string_equals(squ.getClassName().c_str(), "GeoSquare", "Class name should be GeoSquare!");
  test(squ.getAr() == 16 && squ.getCir() == 16, "Something with the calculation went wrong!");
}

void testGeoRectSquare() {
    print("Start test GeoRectSquare:");
  GeoRect *rectSc = new GeoSquare(4);
  rectSc->output();

  GeoSquare sq = GeoSquare(2);
  GeoRect &rectSc2 = sq;
  rectSc2.output();
  string_equals(sq.getClassName().c_str(), "GeoSquare", "Class name should be GeoSquare!");
}

void testGeoTriangle() {
    print("Start test GeoTriangle:");
  GeoTriangle gt = GeoTriangle(1, 2, 3, 5);
  gt.output();
  string_equals(gt.getClassName().c_str(), "GeoTriangle", "Class name should be GeoTriangle!");
  test(gt.getAr() == 7.5 && gt.getCir() == 6, "Something with the calculation went wrong!");
}

void testGeoEllipse() {
    print("Start test GeoEllipse:");
  GeoEllipse ge = GeoEllipse(1, 2);
  ge.output();
  string_equals(ge.getClassName().c_str(), "GeoEllipse", "Class name should be GeoEllipse!");
}

void testGeoCircle() {
    print("Start test GeoCircle:");
  GeoCircle gc = GeoCircle(5);
  gc.output();
  string_equals(gc.getClassName().c_str(), "GeoCircle", "Class name should be GeoCircle!");
}

void testGeoEllipseCircle() {
    print("Start test GeoEllipseCircle:");
  GeoEllipse *geoEllipse = new GeoCircle(4);
  geoEllipse->output();

  GeoCircle gc = GeoCircle(4);
  GeoEllipse &geoEllipse2 = gc;
  geoEllipse2.output();
  string_equals(gc.getClassName().c_str(), "GeoCircle", "Class name should be GeoCircle!");
}

void testGeoContainer() {
    print("Start test GeoContainer:");
  GeoContainer container;

  std::shared_ptr<IGeomObj> rect = std::make_shared<GeoRect>(2, 3);
  std::shared_ptr<IGeomObj> gt = std::make_shared<GeoTriangle>(1, 2, 3, 5);
  std::shared_ptr<IGeomObj> ge = std::make_shared<GeoEllipse>(1, 2);

  container.pushObject(rect);
  container.pushObject(gt);
  container.pushObject(ge);

  string_equals(container.getClassName().c_str(), "GeoContainer", "Class name should be GeoContainer!");

  container.output();
}

void testUniqueID() {
    print("Start test UniqueID:");
   GeoContainer container; 

   std::shared_ptr<IGeomObj> rect = std::make_shared<GeoRect>(2, 3);
   std::shared_ptr<IGeomObj> gt = std::make_shared<GeoTriangle>(1, 2, 3, 5);
   std::shared_ptr<IGeomObj> ge = std::make_shared<GeoEllipse>(1, 2);

   container.pushObject(rect); 
   container.pushObject(gt); 
   container.pushObject(ge); 

   container.output(); 
}

void testCopyConstructor() {
    print("Start test CopyConstructor:");
  GeoSquare square = GeoSquare(4);
  GeoSquare square2 = square;

  GeoCircle circle = GeoCircle(6);
  GeoCircle circle2 = circle;

  GeoTriangle triangle = GeoTriangle(4, 5, 7, 8);
  GeoTriangle triangle2 = triangle;

  // no cctor is called
  IGeomObj *object = new GeoSquare(5);
  IGeomObj *object2 = object;

  object2->output();
}

void testGeoHouse() {
    print("Start test GeoHouse:");
  std::shared_ptr<GeoRect> rect = std::make_shared<GeoRect>(2, 3);
  std::shared_ptr<GeoTriangle> gt = std::make_shared<GeoTriangle>(1, 2, 3, 5);

  GeoHouse gh = GeoHouse(gt, rect);
	gh.output();

    string_equals(gh.getClassName().c_str(), "GeoHouse", "Class name should be GeoHouse!");
    test(gh.getAr() == 13.5 && gh.getCir() == 12, "Something with the calculation went wrong!");
};
