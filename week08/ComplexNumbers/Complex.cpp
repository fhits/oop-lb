#include "Complex.h"

#define REG_EXP "([+-]?[0-9]+)([+-][0-9]+)i"

std::ostream& operator<<(std::ostream& stream, const Complex& complex) {
	if (complex.b > 0) return stream << complex.a << "+" << complex.b << "i";
	return stream << complex.a << complex.b << "i";
}

std::istream& operator>>(std::istream& stream, Complex& complex) {
	std::string string;
	do {
		std::cout << "Please enter your complex number: ";
		stream >> string;
	} while (!complex.input(string));

	return stream;
}


bool Complex::input(std::string inputStr) {
	if (inputStr.empty()) return false;

	std::smatch match;

	std::regex regExp(REG_EXP);

	if(!std::regex_search(inputStr, match, regExp)) return false;

	if (match.length() <= 0) return false;
	this->a = std::stoi(match[1]);
	this->b = std::stoi(match[2]);
	return true;
}
