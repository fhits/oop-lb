#ifndef COMPLEX_H
#define COMPLEX_H

#include <regex>
#include <iostream>
#include <regex>

class Complex {

public:
	Complex() = default;
	Complex(int a, int b) : a{ a }, b{ b } {
	}
	~Complex() = default;

	int a, b;

	Complex operator+(const Complex& complex) const {
		return Complex(this->a + complex.a, this->b + complex.b);
	}

	friend std::ostream& operator<<(std::ostream& stream, const Complex& complex);
	friend std::istream& operator>>(std::istream& istream, Complex& complex);

	bool input(std::string inputStr);
};

#endif // !COMPLEX_H