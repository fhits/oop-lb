#include "Complex.h"

#include <iostream>
#include <cstring>
#include <string>

#define test(check, msg)                                                       \
  if (!(check)) {                                                              \
    std::cerr << "TEST FAILED: " << msg << std::endl;                          \
    exit(EXIT_FAILURE);                                                        \
  }

#define string_equals(string1, string2, msg)                                   \
  test((strcmp(string1, string2) == 0), msg)

#define print(msg)                                                             \
  {                                                                            \
    std::cout << "-------------------------------" << std::endl;               \
    std::cout << msg << std::endl;                                             \
    std::cout << "-------------------------------" << std::endl;               \
  }

void testComplexPlusOp();
void testComplexOutput();
void testRoutine();
void testRegexStatic();
void testComplexInputOperator();


int main() {
	//testComplexPlusOp();
	//testComplexOutput();
	//testRoutine();
	//testRegexStatic();
	testComplexInputOperator();
}


void testComplexPlusOp() {
	print("Test overloaded plus operator!");
	Complex x = Complex(3, 4);
	Complex y = Complex(2, 2);
	Complex ans = x + y;

	test(ans.a == 5 && ans.b == 6, "Complex numbers should be equal!");

	std::cout << ans << std::endl;
}

void testComplexOutput() {
	print("Test overloaded output operator!");
	Complex x = Complex(3, 4);

	std::cout << x << std::endl;
}

void testRoutine() {
	print("Test routine for complex numbers");
	Complex x, y;
	std::string s1, s2;
	std::cin >> s1;
	std::cin >> s2;
	x.input(s1);
	y.input(s2);
	Complex z = x + y;
	std::cout << z << std::endl;
}

void testRegexStatic() {
	print("Test static regular expression");
	Complex x;

	x.input("+6+5i");
	std::cout << x << std::endl;
	x.input("+6-5i");
	std::cout << x << std::endl;
	x.input("-6-5i");
	std::cout << x << std::endl;
	x.input("-6+5i");
	std::cout << x << std::endl;
	x.input("6+5i");
	std::cout << x << std::endl;
	x.input("6-5i");
	std::cout << x << std::endl;
	x.input("0-5i");
	std::cout << x << std::endl;
}
void testComplexInputOperator() {
	print("Test input operator:");
	Complex x;

	std::cin >> x;

	std::cout << x << std::endl;


}
