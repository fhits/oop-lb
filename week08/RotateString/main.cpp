#include <iostream>
#include <iomanip>
#include <string>

std::string operator<<(std::string & inputStr, int number) {
	std::string tempStr(inputStr);
	for (size_t i = 0; i < inputStr.size(); ++i) {
		int currentPos = (int) ((inputStr.size() - number) + i) % inputStr.size();
		tempStr[currentPos] = inputStr.at(i);
	}
	inputStr = std::string{ tempStr };
	return inputStr;
};

std::string & operator>>(std::string &inputStr, int number) {
	std::string tempStr(inputStr);
	for (size_t i = 0; i < inputStr.size(); ++i) {
		int currentPos = (int) ((i + number)) % inputStr.size();
		tempStr[currentPos] = inputStr.at(i);
	}
	inputStr = std::string{ tempStr };
	return inputStr;
};

void testRotateStringLeft(); 
void testRotateStringRight(); 

int main() { 
	testRotateStringLeft();
	testRotateStringRight(); 
}

void testRotateStringLeft() {
	std::string input = "LET IT SNOW";
	std::cout << std::quoted(input << 3) << std::endl;
}

void testRotateStringRight() {
	std::string input = "LET IT SNOW";
	std::cout << std::quoted(input >> 10) << std::endl;
}
