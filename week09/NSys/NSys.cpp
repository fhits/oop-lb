#include "NSys.h"
#include "NSysException.h"
#include <iostream>

int64_t NSys::toDecimal(std::string value) {
  if(value.empty()) throw NSysEmptyEx();
  int64_t decimalValue = 0;
  std::reverse(value.begin(), value.end());
  for (size_t i = 0; i < value.size(); ++i) {
    int tempValue = 0;
    if (value.at(i) <= '9' && value.at(i) >= '0')
      tempValue = value.at(i) - '0';
    if (value.at(i) >= 'A' && value.at(i) <= 'Z')
      tempValue = value.at(i) - 'A'+ 10;
    decimalValue += (tempValue * (int64_t) std::pow(this->base, i));
  }
  if(decimalValue == 0) throw NSysEmptyEx("Calculated decimal value is empty!");
  return decimalValue;
}

std::ostream& operator<<(std::ostream& stream, NSys & nsys) {
  return stream << nsys.toString();
};

std::istream& operator>>(std::istream& stream, NSys & nsys) {
  std::string inputStr;
  stream >> inputStr;
  nsys.parse(inputStr);
  return stream;
};

void NSys::operator>>(int number) {
    this->setValue(this->getValue() >> number);
}

void NSys::operator<<(int number) {
    this->setValue(this->getValue() << number);
}

std::string NSys::toNumberSystem() {

  std::string result = "";

  int64_t tempValue = this->value;

  while(tempValue > 0) {
    int residual = (tempValue %  this->getBase());
    if(residual >= 10) {
      residual = (residual - 10) + 'A';
    } else {
      residual += '0';
    }
    result.append(1, residual);
    tempValue /= this->getBase();
  }

  std::reverse(result.begin(), result.end());

  if(result.empty()) throw NSysEmptyEx("toNumberSystem result is empty!");

  return result;
}

NSys NSys::parse(std::string value) {
  if(value.empty()) throw NSysEmptyEx("Provided value in parse() is empty!");
  NSys newNSys = NSys(*this);
  newNSys.setValue(this->toDecimal(value));
  this->setValue(newNSys.getValue());
  return newNSys;
}

std::string NSys::toString() { 
  return "Number System [" + std::to_string(this->getBase()) + "] with value [" + this->toNumberSystem() + "]";
}

void NSys::operator=(const NSys &other) {
  if(other.getValue() == 0) throw NSysNoValEx("NSys object value is empty!");
  if(other.getBase() <= 0) throw NSysEmptyEx("NSys object base is empty!");
  this->setValue(other.getValue());
  this->setBase(other.getBase());
}

NSys NSys::operator=(const std::string & inputStr) {
  if(inputStr.empty()) throw NSysEmptyEx("Input string is empty!");
  return this->parse(inputStr);
}

NSys NSys::operator+(const NSys& other) {
    NSys tempNSys(this->getBase());
    tempNSys.setValue(this->getValue() + other.getValue());
    return tempNSys;
}

NSys NSys::operator-(const NSys& other) {
    NSys tempNSys(this->getBase());
    tempNSys.setValue(this->getValue() - other.getValue());
    return tempNSys;
}

NSys NSys::operator*(const NSys& other) {
    NSys tempNSys(this->getBase());
    tempNSys.setValue(this->getValue() * other.getValue());
    return tempNSys;
}

NSys NSys::operator/(const NSys& other) {
    NSys tempNSys(this->getBase());
    if (other.getValue() == 0) throw NSysZeroDivisionEx("Division by zero excaption caught!");
    tempNSys.setValue(this->getValue() / other.getValue());
    return tempNSys;
}

void NSys::operator+=(const NSys& other) {
    this->setValue(this->getValue() + other.getValue());
}

void NSys::operator-=(const NSys& other) {
    this->setValue(this->getValue() - other.getValue());
}

void NSys::operator++() {
    this->setValue(this->getValue()+1);
}

NSys NSys::operator++(int postfix) {
    NSys tempNSys(*this);
    this->setValue(this->getValue()+1);
    return tempNSys;
}

void NSys::operator--() {
    this->setValue(this->getValue()-1);
}

NSys NSys::operator--(int postfix) {
    NSys tempNSys(*this);
    this->setValue(this->getValue()-1);
    return tempNSys;
}
