#ifndef NSYS_H
#define NSYS_H

#include "NSysException.h"
#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <string>

#define NUMBERRANGE 36

class NSys {

private:
  int64_t value = 0;
  int base = 0;

public:
  NSys(int base) : base{base} {
      if (base > NUMBERRANGE) {
          throw NSysLengthEx();
      }
  };
  NSys(std::string range) {
    /* TODO: check for binary system!! */
      if (range.size() > NUMBERRANGE) {
          throw NSysLengthEx();
      }
    /* TODO: refactor */
    int asciiCounter = '0';
    for (size_t i = 0; i < range.size(); ++i) {
      if (i == 10)
        asciiCounter = 'A';
      if (!(range.at(i) == asciiCounter)) {
          throw NSysOrderEx();
      }
      asciiCounter++;
    }

    this->base = range.size();
  };

  NSys(std::string range, std::string value) : NSys(range) {
    this->value = toDecimal(value);
  };

  NSys(int base, std::string value) : NSys(base) {
    this->value = toDecimal(value);
  };

  void operator=(const NSys &other);
  NSys operator=(const std::string & inputStr);
  NSys operator+(const NSys &other);
  NSys operator-(const NSys &other);
  NSys operator*(const NSys &other);
  NSys operator/(const NSys &other);
  void operator+=(const NSys &other);
  void operator-=(const NSys &other);
  void operator++();
  NSys operator++(int postfix);
  void operator--();
  NSys operator--(int postfix);

  friend std::istream& operator>>(std::istream & stream, NSys& nsys);

  friend std::ostream& operator<<(std::ostream & stream, NSys& nsys);
  
  void operator>>(int number);

  void operator<<(int number);

  int64_t toDecimal(std::string value);

  std::string toNumberSystem();

  std::string toString();

  NSys parse(std::string value);

  const int64_t getValue() const { return this->value; }
  const int getBase() const { return this->base; }

protected:
  void setValue(int64_t value) { this->value = value; }
  void setBase(int base) { this->base = base; }
};



#endif /* ifndef NSYS_H */
