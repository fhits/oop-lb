#include "NSys.h"

class NSys10 : public NSys {

public:
  NSys10() = delete;
  NSys10(std::string value) : NSys(10, value){};
};
