#include "NSys.h"

class NSys16 : public NSys {

public:
  NSys16() = delete;
  NSys16(std::string value) : NSys(16, value){};
};
