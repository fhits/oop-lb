#include "NSys.h"

class NSys2 : public NSys {

public:
  NSys2() = delete;
  NSys2(std::string value) : NSys(2, value){};
};
