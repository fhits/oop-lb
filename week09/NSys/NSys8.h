#include "NSys.h"

class NSys8 : public NSys {

public:
  NSys8() = delete;
  NSys8(std::string value) : NSys(8, value){};
};
