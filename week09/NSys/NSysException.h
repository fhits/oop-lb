#ifndef NSYSEXCEPTION
#define NSYSEXCEPTION

#include <iostream>

struct NSysException : public std::exception {

protected:
  std::string message = "";
public:
  NSysException(std::string msg) : message(msg){};
  NSysException() { this->message = "NSysException caught!"; }

  virtual const char *what() const throw() { return this->message.c_str(); }
};

struct NSysLengthEx : public NSysException {
  NSysLengthEx() { this->message = "NSysLengthEx caught!"; }
  NSysLengthEx(std::string msg) : NSysException(msg){};

  const char *what() const throw() override { return this->message.c_str(); }
};

struct NSysOrderEx : public NSysException {
  NSysOrderEx() { this->message = "NSysOrderEx caught!"; }
  NSysOrderEx(std::string msg) : NSysException(msg){};

  const char *what() const throw() override { return this->message.c_str(); }
};

struct NSysEmptyEx : public NSysException {
  NSysEmptyEx() { this->message = "NSysEmptyEx caught!"; }
  NSysEmptyEx(std::string msg) : NSysException(msg){};

  const char *what() const throw() override { return this->message.c_str(); }
};

struct NSysNoValEx : public NSysException {
  NSysNoValEx() { this->message = "NSysNoValEx caught!"; }
  NSysNoValEx(std::string msg) : NSysException(msg){};

  const char *what() const throw() override { return this->message.c_str(); }
};

struct NSysZeroDivisionEx : public NSysException {
  NSysZeroDivisionEx() { this->message = "NSysZeroDivisionEx caught!"; }
  NSysZeroDivisionEx(std::string msg) : NSysException(msg){};

  const char *what() const throw() override { return this->message.c_str(); }
};

#endif // !NSYSEXCEPTION
