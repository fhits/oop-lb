#include "NSys.h"
#include "NSys10.h"
#include "NSys16.h"
#include "NSys2.h"
#include "NSys8.h"
#include "NSysException.h"
#include <cassert>
#include <sstream>

void testNSysCreationWithBase();
void testNSysCreationWithRange();
void testNSysBase();
void testNSysDecimal();
void testNSysAssignment();
void testNSysStringAssigment();
void testToString();
void testParse();
void testOutputOperator();
void testInputOperator();

void testPlusOperator();
void testMinusOperator();
void testMultiplyOperator();
void testDivisionOperator();
void testPlusAssignOperator();
void testMinusAssignOperator();
void testPlusPlusPreOperator();
void testPlusPlusPostOperator();
void testMinusMinusPreOperator();
void testMinusMinusPostOperator();

void testBitShiftOperator();

void testNSys10Creation();
void testNSys16Creation();
void testNSys8Creation();
void testNSys2Creation();

int main(int argc, char *argv[]) {
  try {
    testNSysDecimal();
    testNSysCreationWithBase();
    testNSysCreationWithRange();
    testNSysAssignment();
    testNSysStringAssigment();
    testPlusOperator();
    testToString();
    testParse();
    testOutputOperator();
    testInputOperator();
    testMinusOperator();
    testMultiplyOperator();
    testDivisionOperator();
    testPlusAssignOperator();
    testMinusAssignOperator();
    testPlusPlusPreOperator();
    testPlusPlusPostOperator();
    testMinusMinusPreOperator();
    testMinusMinusPostOperator();
    testBitShiftOperator();
    testNSys10Creation();
    testNSys16Creation();
    testNSys8Creation();
    testNSys2Creation();

  } catch (NSysLengthEx &e) {
    std::cerr << e.what() << std::endl;
    exit(EXIT_FAILURE);
  } catch (NSysOrderEx &e) {
    std::cerr << e.what() << std::endl;
    exit(EXIT_FAILURE);
  } catch (NSysException &e) {
    std::cerr << e.what() << std::endl;
    exit(EXIT_FAILURE);
  }
  
  return 0;
}

void testNSysCreationWithBase() {
  NSys baseNsys = NSys(20);
  assert(baseNsys.getBase() == 20);
}

void testNSysCreationWithRange() {
  std::string inputStr = "0123456789A";
  NSys baseNsys = NSys(inputStr);
  assert(baseNsys.getBase() == 11);
}

void testNSysBase() {
  std::string inputStr = "0123456789ABCDEFGHIJK";
  NSys baseNsys = NSys(inputStr);
  assert(baseNsys.getBase() == static_cast<int>(inputStr.size()));
}

void testNSysDecimal() {
  NSys baseNsys = NSys(5, "3142");
  NSys baseNsys2 = NSys(10, "110011");

  assert(baseNsys.getValue() == 422);
  assert(baseNsys2.getValue() == 110011);
}

void testNSysAssignment() {
  NSys binarySystem = NSys(2, "111");
  NSys octalSystem = NSys(8, "100");
  octalSystem = binarySystem;

  assert(octalSystem.getValue() == 7);
  assert(octalSystem.getBase() == 2);
}

void testNSysStringAssigment() {
  NSys stringSystem(2); 
  stringSystem = "1011";
  assert(stringSystem.getValue() == 11);
}

void testToString() {
  NSys testSystem = NSys(8, "1234567812");
  testSystem.toString();
}

void testParse() {
  NSys parseSystem = NSys(2);
  assert(parseSystem.parse("101111").getValue() == 47);
}

void testOutputOperator() {
  NSys someNSys = NSys(10, "1AA");
  std::cout << someNSys << std::endl;
}

void testInputOperator() {
  NSys someNSys = NSys(16);
  std::stringstream("578") >> someNSys;
  std::cout << someNSys.toString() << std::endl;
}

void testPlusOperator() {
  NSys binarySystem = NSys(2, "111");
  NSys octalSystem = NSys(8, "100");
  NSys newSys = octalSystem + binarySystem;

  assert(newSys.getValue() == 71);
  assert(newSys.getBase() == 8);
}

void testMinusOperator() {
  NSys binarySystem = NSys(2, "111");
  NSys octalSystem = NSys(8, "100");
  NSys newSys = octalSystem - binarySystem;

  assert(newSys.getValue() == 57);
  assert(newSys.getBase() == 8);
}

void testDivisionOperator() {
  NSys binarySystem = NSys(2, "111");

  NSys octalSystem = NSys(8, "100");
  NSys newSys = octalSystem * binarySystem;

  assert(newSys.getValue() == 448);
  assert(newSys.getBase() == 8);
}

void testMultiplyOperator() {
  NSys binarySystem = NSys(2, "111");
  NSys octalSystem = NSys(8, "100");
  NSys newSys = octalSystem / binarySystem;

  assert(newSys.getValue() == 9);
  assert(newSys.getBase() == 8);
}

void testPlusAssignOperator() {
  NSys binarySystem = NSys(2, "111");
  NSys octalSystem = NSys(8, "100");
  octalSystem += binarySystem;

  assert(octalSystem.getValue() == 71);
  assert(octalSystem.getBase() == 8);
}

void testMinusAssignOperator() {
  NSys binarySystem = NSys(2, "111");
  NSys octalSystem = NSys(8, "100");
  octalSystem -= binarySystem;

  assert(octalSystem.getValue() == 57);
  assert(octalSystem.getBase() == 8);
}

void testPlusPlusPreOperator() {
  NSys binarySystem = NSys(2, "111");
  ++binarySystem;

  assert(binarySystem.getValue() == 8);
  assert(binarySystem.getBase() == 2);
}

void testPlusPlusPostOperator() {
  NSys binarySystem = NSys(2, "111");
  binarySystem++;

  assert(binarySystem.getValue() == 8);
  assert(binarySystem.getBase() == 2);
}

void testMinusMinusPreOperator() {
  NSys binarySystem = NSys(2, "111");
  --binarySystem;

  assert(binarySystem.getValue() == 6);
  assert(binarySystem.getBase() == 2);
}

void testMinusMinusPostOperator() {
  NSys binarySystem = NSys(2, "111");
  binarySystem--;

  assert(binarySystem.getValue() == 6);
  assert(binarySystem.getBase() == 2);
}
void testBitShiftOperator() {
  NSys octalSystem = NSys(8, "523");
  NSys dualSystem = NSys(2, "11001");

  octalSystem << 2;
  octalSystem >> 2;

  dualSystem << 2;
  assert(dualSystem.getValue() == 100);
  dualSystem >> 2;
  assert(dualSystem.getValue() == 25);
}

void testNSys10Creation() {
  NSys10 nsysTen = NSys10("1234");
  assert(nsysTen.getBase() == 10);
  assert(nsysTen.getValue() == 1234);
}

void testNSys16Creation() {
  NSys16 nsysSixteen = NSys16("AB12A2");
  assert(nsysSixteen.getBase() == 16);
  assert(nsysSixteen.getValue() == 11211426);
}

void testNSys8Creation() {
  NSys8 nsysEight = NSys8("4522");
  assert(nsysEight.getBase() == 8);
  assert(nsysEight.getValue() == 2386);
}

void testNSys2Creation() {
  NSys2 nsysTwo = NSys2("111010101");
  assert(nsysTwo.getBase() == 2);
  assert(nsysTwo.getValue() == 469);
}
