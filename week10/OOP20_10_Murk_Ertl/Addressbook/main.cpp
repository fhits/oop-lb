#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <regex>
#include <tuple>
#include <vector>

#define PERSON_FILENAME "personen.csv"
#define SVN_FILENAME "svn.csv"

#define REG_EXP "(.*)[,]([0-9]*)[,](.*)[,](.*$)"

#define error(msg) std::cerr << msg << std::endl;
#define print(msg) std::cout << msg << std::endl;

typedef std::tuple<std::string, unsigned long long, std::string, std::string>
    personTuple;

typedef std::map<std::string, personTuple> PersonMap;

typedef std::vector<std::string> StringVector;

struct Exception : public std::exception {
protected:
  std::string message = "";

public:
  Exception(std::string msg) : message(msg){};
  Exception() { this->message = "Exception caught!"; }

  virtual const char *what() const throw() { return this->message.c_str(); }
};

StringVector readCSVFile(std::string fileName) {
  StringVector lines;
  std::ifstream filePtr(fileName);
  char delimiter = '\n';

  if (!filePtr) {
    Exception fileptrexp("Exception: no file provided or error");
    throw fileptrexp;
  }

  if (filePtr.is_open()) {
    std::string line;
    while (std::getline(filePtr, line, delimiter)) {
      lines.push_back(line);
    }
  }
  return lines;
}

auto parsePersonString(const StringVector &stringVector, std::string regExpr) {
  std::smatch match;

  std::regex regExp(regExpr);

  std::vector<personTuple> vectorTuple;

  auto lambda = [&](std::string person) -> void {
    if (!std::regex_search(person, match, regExp)) {
      Exception fileptrexp("Exception: regex not ready");
      throw fileptrexp;
    }

    std::string name = match[1];

    /* TODO: find better way to convert */
    std::string tmp = match[2];
    unsigned long long telephone = std::stoll(tmp.c_str(), nullptr, 0);

    std::string street = match[3];
    std::string address = match[4];

    vectorTuple.push_back(std::make_tuple(name, telephone, street, address));
  };

  std::for_each(stringVector.begin(), stringVector.end(), lambda);

  return vectorTuple;
}

void printTuple(const std::vector<personTuple> &tupleVector) {

  for (auto singleTuple : tupleVector) {
    std::cout << "Name: [" << std::get<0>(singleTuple) << "] ";
    std::cout << "Tel: [" << std::get<1>(singleTuple) << "] ";
    std::cout << "Street: [" << std::get<2>(singleTuple) << "] ";
    std::cout << "Address: [" << std::get<3>(singleTuple) << "]" << std::endl;
  }
}


void fillMap(PersonMap &personMap, StringVector &stringVector,
             const std::vector<personTuple> &vectorTuple) {
  int i = 0;
  for (auto svn : stringVector) {
    personMap.insert(std::make_pair(svn, vectorTuple[i++]));
  }
}

void printMap(PersonMap &personMap) {

  if (personMap.empty()) {
    Exception fileptrexp("Exception: PersonMap is empty");
    throw fileptrexp;
  }

  for (auto it = personMap.begin(); it != personMap.end(); it++) {
    std::cout << it->first << "::" << std::get<0>(it->second) << std::endl;
  }
}

int main(int argc, char *argv[]) {
  try {
    StringVector personStrings = readCSVFile(PERSON_FILENAME);
    personStrings.erase(personStrings.begin());

    auto vectorTuple = parsePersonString(personStrings, REG_EXP);

    printTuple(vectorTuple);

    StringVector svnStrings = readCSVFile(SVN_FILENAME);
    svnStrings.erase(svnStrings.begin());

    PersonMap personMap;

    fillMap(personMap, svnStrings, vectorTuple);

    printMap(personMap);
  } catch (Exception &e) {
    std::cerr << e.what() << std::endl;
    exit(EXIT_FAILURE);
  }
  return 0;
}
