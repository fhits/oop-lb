#include <functional>
#include <iostream>

int main(int argc, char *argv[]) {
  int a = 15;
  int b = 2;

  auto lambda = [=](auto a, int b, int op) -> double {
    switch (op) {
    case '+':
      return a + b;
    case '-':
      return a - b;
    case '*':
      return a * b;
    case '/':
      return static_cast<double>(a) / b;
    default:
      return 0;
    }
  };

  double result1 = lambda(a, b, '+');
  double result2 = lambda(a, b, '-');
  double result3 = lambda(a, b, '*');
  double result4 = lambda(a, b, '/');

  std::cout << result1 << std::endl;
  std::cout << result2 << std::endl;
  std::cout << result3 << std::endl;
  std::cout << result4 << std::endl;

  return 0;
}
