#include <vector>
#include <algorithm>
#include <iostream>

int main(int argc, char *argv[]) {


	std::vector<int> vec{ 2,5,3,2,1,10,12,52,264 };

	int sumOdd = 0;
	int sumEven = 0;
	int sumGreater = 0;
	auto check = [&](auto val) -> void {
		//std::find_if(vec.begin(), vec.end(), [&](int value) {
		//	return value > vec.at(6)
		//	});
		(val % 2 == 0) ? sumEven++ : sumOdd++;
		(val > vec.at(6)) ? sumGreater++ : sumGreater;
		(vec.back() == val) ? std::cout << val << std::endl : std::cout << val << " ";

	};


	std::for_each(vec.begin(), vec.end(), check);
	
	std::cout << "Sum of - even values: " << sumEven << " odd values: " << sumOdd << " greater values: " << sumGreater << std::endl;

	
}