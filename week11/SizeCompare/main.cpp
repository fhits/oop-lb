#include<iostream>

#define print(msg) { std::cout << "================" << msg << "================" <<std::endl; }

void testIntInt();
void testStringInt();
void testStringDouble();
void testStringFloat();
void testDoubleInt();
void testCharInt();

template<typename T, typename X> int sizeCompare(T a, X b) {
	int returnVal = 404;
	std::cout << "Sizes: " << sizeof(a) << " " << sizeof(b) << std::endl;
	
	if (sizeof(a) > sizeof(b)) {
		returnVal = 1;
	}
	if (sizeof(a) == sizeof(b)) {
		returnVal = 0;
	}
	
	if (sizeof(a) < sizeof(b)) {
		returnVal = -1;
	}
	return returnVal;
}


int main() {

	testIntInt();
	testStringInt();
	testStringDouble();
	testStringFloat();
	testDoubleInt();
	testCharInt();

	return 0;
}

void testIntInt() {
	print("Compare int with int");
	std::cout << sizeCompare(35, 3) << std::endl;
}

void testStringInt() {
	print("Compare string with int");
	std::cout << sizeCompare("Test", 3) << std::endl;
}  

void testStringDouble() {
	print("Compare string with double");
	std::cout << sizeCompare("Test", 3.4) << std::endl;
}

void testStringFloat() {
	print("Compare string with float");
	std::cout << sizeCompare("Test", 3.4f) << std::endl;
}

void testDoubleInt() {
	print("Compare double with int");
	std::cout << sizeCompare(2.4, 3) << std::endl;
}

void testCharInt() {
	print("Compare char with int");
	std::cout << sizeCompare('t', 3) << std::endl;
}
