#include <algorithm>
#include <array>
#include <iostream>
#include <queue>
#include <string>
#include <vector>

template <typename T> std::array<T, 3> sortThree(T a, T b, T c) {
  std::array<T, 3> sorted = {a, b, c};
  // std::cout << a << " " << b << " " << c << std::endl;

  std::sort(sorted.begin(), sorted.end(), std::greater<T>());

  return sorted;
}

int main() {

  std::array<int, 3> ar1 = sortThree<int>(2, 4, 2);
  for (int element : ar1) {
    std::cout << element << " ";
  }

  std::cout << "" << std::endl;
  std::array<double, 3> ar2 = sortThree(4.2, 5.3, 5.2);
  for (double element : ar2) {
    std::cout << element << " ";
  }

  std::cout << "" << std::endl;
  std::array<char, 3> ar3 = sortThree('t', 'w', 'a');
  for (char element : ar3) {
    std::cout << element << " ";
  }

  std::cout << "" << std::endl;
  std::array<char const *, 3> ar4 = sortThree("tr", "rq", "ata");
  for (char const *element : ar4) {
    std::cout << *element << " ";
  }

  std::vector<int> a = {3, 2, 5};
  std::vector<int> b = {4, 20, 5};
  std::vector<int> c = {9, 3, 1};

  std::cout << "" << std::endl;
  std::array<std::vector<int>, 3> ar5 = sortThree(a, b, c);
  for (std::vector<int> element : ar5) {
    for (int el : element) {
      std::cout << el << " ";
    }
    std::cout << std::endl;
  }

  std::queue<int> a1;
  a1.push(3);
  a1.push(2);
  a1.push(5);
  std::queue<int> b1;
  b1.push(4);
  b1.push(2);
  b1.push(6);
  std::queue<int> c1;
  c1.push(9);
  c1.push(3);
  c1.push(4);

  std::cout << "" << std::endl;
  std::array<std::queue<int>, 3> ar6 = sortThree(a1, b1, c1);
  for (std::queue<int> element : ar6) {
    while (!element.empty()) {
      std::cout << element.front() << " ";
      element.pop();
    }
    std::cout << std::endl;
  }

  return 0;
}
