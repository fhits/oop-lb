#include <algorithm>
#include <array>
#include <iostream>

#define print(msg)                                                             \
  { std::cout << "================" << msg << "================" << std::endl; }

void testWithInt();
void testWithDouble();
void testWithFloat();
void testWithChar();
void testWithBool();
void testWithString();

template <typename T> bool swapThree(T &a, T &b, T &c) {
  auto lambda = [](T &a, T &b) {
    if (a > b) {
      T temp(a);
      a = b;
      b = temp;
      return true;
    }
    return false;
  };

  bool testOne = lambda(a, b);
  bool testTwo = lambda(a, c);
  bool testThree = lambda(b, c);

  return (testOne || testTwo || testThree);
}

int main(int argc, char *argv[]) {
  testWithInt();
  testWithDouble();
  testWithFloat();
  testWithChar();
  testWithBool();
  testWithString();
  return 0;
}

void testWithInt() {
  print(" Integer Test: ") int a = 4, b = 3, c = 2;

  std::cout << (swapThree(a, b, c) ? "VALUES HAVE BEEN SWAPPED"
                                   : "NO VALUES HAVE BEEN SWAPPED")
            << std::endl;

  std::cout << a << " " << b << " " << c << std::endl;
}

void testWithDouble() {
  print(" Double Test: ") double a = 4.7, b = 4.6, c = 4.9;

  std::cout << (swapThree(a, b, c) ? "VALUES HAVE BEEN SWAPPED"
                                   : "NO VALUES HAVE BEEN SWAPPED")
            << std::endl;

  std::cout << a << " " << b << " " << c << std::endl;
}

void testWithFloat() {
  print(" Float Test: ") float a = 2.5f, b = 2.6f, c = 4.9f;

  std::cout << (swapThree(a, b, c) ? "VALUES HAVE BEEN SWAPPED"
                                   : "NO VALUES HAVE BEEN SWAPPED")
            << std::endl;

  std::cout << a << " " << b << " " << c << std::endl;
}

void testWithChar() {
  print(" Char Test: ") char a = 'y', b = 'e', c = 's';

  std::cout << (swapThree(a, b, c) ? "VALUES HAVE BEEN SWAPPED"
                                   : "NO VALUES HAVE BEEN SWAPPED")
            << std::endl;

  std::cout << a << " " << b << " " << c << std::endl;
}

void testWithBool() {
  print(" Bool Test: ") bool a = true, b = false, c = false;

  std::cout << (swapThree(a, b, c) ? "VALUES HAVE BEEN SWAPPED"
                                   : "NO VALUES HAVE BEEN SWAPPED")
            << std::endl;

  std::cout << a << " " << b << " " << c << std::endl;
}

void testWithString() {
  print(" String Test: ") std::string a = "frenz", b = "seppl", c = "franz";

  std::cout << (swapThree(a, b, c) ? "VALUES HAVE BEEN SWAPPED"
                                   : "NO VALUES HAVE BEEN SWAPPED")
            << std::endl;

  std::cout << a << " " << b << " " << c << std::endl;
}
