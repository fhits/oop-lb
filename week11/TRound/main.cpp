#include <cmath>
#include <iostream>

void testWithInt();
void testWithNegativeInt();
void testWithDouble();
void testWithFloat();
void testWithLong();

#define print(msg)                                                             \
  { std::cout << "============= " << msg << " =============" << std::endl; }

struct Exception : public std::exception {

protected:
  std::string message = "";

public:
  Exception(std::string msg) : message(msg){};
  Exception() { this->message = "Exception caught!"; }

  virtual const char *what() const throw() { return this->message.c_str(); }
};

template <typename T> T tRound(T a, int digits) {
  if (typeid(a) == typeid(int) && digits < 0) {
    Exception digitEx("digits must be positive for int!");
    throw digitEx;
  }

  T temp = a;
  int count = 0;
  while ((temp * temp) >= 1) {
    temp /= 10;
    count++;
  }

  if (count <= digits)
    digits = count - 1;

  T power = std::pow(10, digits);
  return (std::round(a / power) * power);
}

int main(int argc, char *argv[]) {
  try {
    testWithInt();
    testWithNegativeInt();
    testWithDouble();
    testWithFloat();
    testWithLong();
  } catch (Exception &ex) {
    std::cerr << ex.what() << std::endl;
    exit(EXIT_FAILURE);
  }
  return 0;
}

void testWithInt() {
  print("Integer Test");
  int a = 46;
  std::cout << tRound(a, 2) << std::endl;
  std::cout << tRound(a, 1) << std::endl;
  std::cout << tRound(a, 0) << std::endl;
}

void testWithNegativeInt() {
  print("Negative Integer Test");
  int a = -2346;
  std::cout << tRound(a, 3) << std::endl;
  std::cout << tRound(a, 1) << std::endl;
  std::cout << tRound(a, 0) << std::endl;
}
void testWithDouble() {
  print("Double Test");
  double a = 112.52953;
  std::cout << tRound(a, 2) << std::endl;
  std::cout << tRound(a, 1) << std::endl;
  std::cout << tRound(a, 0) << std::endl;
  std::cout << tRound(a, -1) << std::endl;
  std::cout << tRound(a, -2) << std::endl;
  std::cout << tRound(a, -3) << std::endl;
  std::cout << tRound(a, -4) << std::endl;
  std::cout << tRound(a, -5) << std::endl;
}

void testWithFloat() {
  print("Float Test");
  float a = 33419.529953f;
  std::cout << tRound(a, 2) << std::endl;
  std::cout << tRound(a, 1) << std::endl;
  std::cout << tRound(a, 0) << std::endl;
  std::cout << tRound(a, -1) << std::endl;
  std::cout << tRound(a, -2) << std::endl;
  std::cout << tRound(a, -3) << std::endl;
  std::cout << tRound(a, -4) << std::endl;
  std::cout << tRound(a, -5) << std::endl;
}

void testWithLong() {
  print("Long Integer Test");
  long int a = 33419658L;
  std::cout << tRound(a, 0) << std::endl;
  std::cout << tRound(a, 1) << std::endl;
  std::cout << tRound(a, 2) << std::endl;
  std::cout << tRound(a, 3) << std::endl;
  std::cout << tRound(a, 4) << std::endl;
  std::cout << tRound(a, 5) << std::endl;
  std::cout << tRound(a, 6) << std::endl;
}
