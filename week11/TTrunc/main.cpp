#include <cmath>
#include <iomanip>
#include <iostream>
#include <typeinfo>

#define print(msg)                                                             \
  { std::cout << "================" << msg << "================" << std::endl; }

struct Exception : public std::exception {
protected:
  std::string message = "";

public:
  Exception(std::string msg) : message(msg){};
  Exception() { this->message = "Exception caught!"; }

  virtual const char *what() const throw() { return this->message.c_str(); }
};
template <class T>
typename std::enable_if<std::is_arithmetic<T>::value, T>::type
tTrunc(T a, int b) { 

  int count = 0;
  T temp = a;

  while ((temp * temp) >= 1) {
    temp /= 10;
    count++;
  }
  std::cout << count << std::endl;

  if (count <= b) {
    b = count - 1;
  }

  T power = std::pow(10, b);
  return (std::floor(a / power) * power);
};

void testIntTrunc();
void testUnsignedIntTrunc();
void testDoubleTrunc();
void testFloatTrunc();

int main() {

  try {
    std::cout << std::setprecision(10);
    testIntTrunc();
    testUnsignedIntTrunc();
    testDoubleTrunc();
    testFloatTrunc();
  } catch (Exception &e) {
    std::cerr << e.what() << std::endl;
    exit(EXIT_FAILURE);
  }

  return 0;
}

void testIntTrunc() {
  print("Trunc an int");
  std::cout << tTrunc(-8543, 5) << std::endl;
}

void testUnsignedIntTrunc() {
  print("Trunc an unsigned int");
  unsigned int a = 8543;
  std::cout << tTrunc(a, 2) << std::endl;
}

void testDoubleTrunc() {
  print("Trunc an double");
  double a = 8543.456;
  std::cout << tTrunc(a, -2) << std::endl;
}

void testFloatTrunc() {
  print("Trunc an float");
  double a = 8543.456f;
  std::cout << tTrunc(a, -3) << std::endl;
}
