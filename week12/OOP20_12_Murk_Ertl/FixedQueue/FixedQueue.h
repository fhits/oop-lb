#ifndef FIXEDQUEUE_H
#define FIXEDQUEUE_H

#include "QueueException.h"


template <typename T, int size>
class FixedQueue {
private:
	int max_size{ size }, current_size{ 0 }, current_front{ 0 }, rear{ 0 };
	T values[size]{};
public:
	FixedQueue() = default;

	const int get_max_size() const { return this->max_size; }
	const int get_size() const { return this->current_size; }
	void increase_size() { this->current_size++; }
	void decrease_size() { this->current_size--; }
	const int get_current_front() const { return this->current_front; }
	void increase_current_front() { this->current_front++; }
	const int get_rear() const { return this->rear; }
	void increase_rear() { this->rear++; }
	const bool empty() const { return this->current_size == 0; }
	const bool full() const { return this->current_size == this->max_size; }

	const T front() const {
		if (this->empty()) throw QueueEmptyException();
		return this->values[this->get_current_front()];
	}

	void push(T value) {
		this->values[this->get_rear() % this->get_max_size()] = value;
		(full()) ? this->increase_current_front() : this->increase_size();
		this->increase_rear();
	}

	void pop() {
		if (this->empty()) throw QueueEmptyException();
		this->increase_current_front();
		this->decrease_size();
	}

};


#endif // !FIXEDQUEUE_H
