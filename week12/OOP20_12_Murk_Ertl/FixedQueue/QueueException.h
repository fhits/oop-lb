#pragma once

#include <iostream>

struct QueueException : public std::exception {

	std::string message;
	QueueException() = default;
	QueueException(std::string msg) : message(msg) {};

	virtual const char* what() const throw () {
		return this->message.c_str();
	}

};

struct QueueEmptyException : public QueueException {

	QueueEmptyException() = default;

	const char* what() const throw () override {
		return "Queue is empty!";
	}
};

