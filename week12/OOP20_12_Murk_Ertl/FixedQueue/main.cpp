#include <iostream>
#include <cassert>
#include <string>
#include <typeinfo>
#include <vector>
#include "QueueException.h"
#include "FixedQueue.h"

void createFixedQueue();
void isEmptyQueue();
void addValuesToFixedQueue();
void removeValuesFromFixedQueue();
void overrideValues();
void createFixedQueueDouble();
void addValuesToFixedQueueDouble();
void addValuesToFixedQueueFloat();
void addValuesToFixedQueueString();
void addValuesToFixedQueueVector();

int main() {
	try {
		createFixedQueue();
		isEmptyQueue();
		addValuesToFixedQueue();
		removeValuesFromFixedQueue();
		overrideValues();
		createFixedQueueDouble();
		addValuesToFixedQueueDouble();
		addValuesToFixedQueueFloat();
		addValuesToFixedQueueString();
		addValuesToFixedQueueVector();
	}
	catch (QueueEmptyException& qe) {
		std::cerr << qe.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	catch (QueueException& e) {
		std::cerr << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	catch (...) {
		exit(EXIT_FAILURE);

	}

	return 0;
}

void createFixedQueue() {

	FixedQueue<int, 10> queue;

	assert(queue.get_max_size() == 10);
}

void isEmptyQueue() {

	FixedQueue<int, 20> queue;

	assert(queue.empty() == true);
}

void addValuesToFixedQueue() {

	int value = 5;
	FixedQueue<int, 10> queue;
	queue.push(value);

	assert(queue.get_size() == 1);
	assert(queue.front() == value);
}

void removeValuesFromFixedQueue() {

	int value = 5;
	FixedQueue<int, 10> queue;
	queue.push(value);
	queue.push((value + 5));

	queue.pop();
	assert(queue.get_size() == 1);
	assert(queue.front() == (value + 5));
}

void overrideValues() {
	const int max_size = 10;
	FixedQueue<int, max_size> queue;
	for (int i = 0; i < max_size; i++) {
		queue.push(i + 1);
	}

	assert(queue.get_size() == max_size);

	queue.push(100);

	assert(queue.front() == 2);
}

void createFixedQueueDouble() {
	FixedQueue<double, 10> queue;

	assert(queue.get_max_size() == 10);
}

void addValuesToFixedQueueDouble() {
	double value = 5.2;
	FixedQueue<double, 10> queue;
	queue.push(value);

	assert(queue.get_size() == 1);
	assert(queue.front() == value);
	assert(typeid(queue.front()) == typeid(value));
}

void addValuesToFixedQueueFloat() {
	float value = 5.2f;
	FixedQueue<float, 10> queue;
	queue.push(value);

	assert(queue.get_size() == 1);
	assert(queue.front() == value);
	assert(typeid(queue.front()) == typeid(value));
}

void addValuesToFixedQueueString() {
	std::string value = "test";
	FixedQueue<std::string, 10> queue;
	queue.push(value);

	assert(queue.get_size() == 1);
	assert(queue.front() == value);
	assert(typeid(queue.front()) == typeid(value));
}

void addValuesToFixedQueueVector() {
	std::vector<int> value = { 2,4,5 };
	FixedQueue<std::vector<int>, 10> queue;
	queue.push(value);

	assert(queue.get_size() == 1);
	assert(queue.front() == value);
	assert(typeid(queue.front()) == typeid(value));
}
