#ifndef COMPX_H
#define COMPX_H

#include <regex>
#include <iostream>
#include <math.h>

namespace MuEr {

	template <class T>
	class CompX {
	private:
		T a, b;

	public:
		CompX() = default;
		CompX(T a, T b) : a{ a }, b{ b } { };
		~CompX() = default;

		auto getSquare(T a, T b) const {
			return (sqrt(pow(a, 2) + pow(b, 2)));
		}

		CompX operator+(const CompX& complex) const {
			return CompX(this->a + complex.a, this->b + complex.b);
		}

		CompX operator-(const CompX& complex) const {
			return CompX(this->a - complex.a, this->b - complex.b);
		}

		CompX operator*(const CompX& complex) const {
			return CompX(this->a * complex.a, this->b * complex.b);
		}

		CompX operator/(const CompX& complex) const {
			return CompX(this->a / complex.a, this->b / complex.b);
		}

		bool operator==(const CompX& complex) const{
			return ((this->a == complex.a) && (this->b == complex.b));
		}

		bool operator>(CompX& complex) const {
			return (getSquare(this->a, this->b) > getSquare(complex.a, complex.b));
		}

		bool operator<(CompX& complex) const {
			return (getSquare(this->a, this->b) < getSquare(complex.a, complex.b));
		}

		CompX& operator=(const CompX& complex) noexcept {
			if (this != &complex) {
				this->a = complex.a;
				this->b = complex.b;
			}
			return *this;
		}
			
	const T getA() const {
		return this->a;
	}

	const T getB() const {
		return this->b;
	}
	
	T setA(const T a) {
		this->a = a;
	}

	T setB(const T b) {
		this->b = b;
	}

	friend std::ostream& operator<<(std::ostream& stream, const CompX<T>& complex) {
		if (complex.getB() > 0) return stream << complex.getA() << "+" << complex.getB() << "i";
		return stream << complex.getA() << complex.getB() << "i";
	}

};

#endif // !COMPX_H

}
