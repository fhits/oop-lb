#include <iostream>
#include "CompX.h"

//using MuEr::CompX;

#define print(msg) { std::cout << "====================== " << msg << " ======================" << std::endl; }

void addComplexNumbers();
void subtractComplexNumbers();
void multiplyComplexNumbers();
void divComplexNumbers();
void comparisonComplexNumbers();
void greaterComplexNumbers();
void lessComplexNumbers();
void assignComplexNumbers();

int main() {

	addComplexNumbers();
	subtractComplexNumbers();
	multiplyComplexNumbers();
	divComplexNumbers();
	comparisonComplexNumbers();
	greaterComplexNumbers();
	lessComplexNumbers();
	assignComplexNumbers();

	return 0;

}

void addComplexNumbers() {
	print("Add Complex Numbers");
	MuEr::CompX<int> complexOne = MuEr::CompX<int>(10, 15);
	MuEr::CompX<int> complexTwo = MuEr::CompX<int>(5, 1);
	MuEr::CompX<int> complexThree = complexOne + complexTwo;

	std::cout << complexThree << std::endl;
}


void subtractComplexNumbers() {
	print("Subtract Complex Numbers");
	MuEr::CompX<int> complexOne = MuEr::CompX<int>(10, 15);
	MuEr::CompX<int> complexTwo = MuEr::CompX<int>(5, 1);
	MuEr::CompX<int> complexThree = complexOne - complexTwo;

	std::cout << complexThree << std::endl;
}

void multiplyComplexNumbers(){
	print("Multiply Complex Numbers");
	MuEr::CompX<int> complexOne = MuEr::CompX<int>(10, 15);
	MuEr::CompX<int> complexTwo = MuEr::CompX<int>(5, 1);
	MuEr::CompX<int> complexThree = complexOne * complexTwo;

	std::cout << complexThree << std::endl;
}

void divComplexNumbers(){
	print("Divide Complex Numbers");
	MuEr::CompX<int> complexOne = MuEr::CompX<int>(10, 15);
	MuEr::CompX<int> complexTwo = MuEr::CompX<int>(3, 2);
	MuEr::CompX<int> complexThree = complexOne / complexTwo;

	std::cout << complexThree << std::endl;
}

void comparisonComplexNumbers(){
	print("Comparison Complex Numbers");
	MuEr::CompX<int> complexOne = MuEr::CompX<int>(10, 15);
	MuEr::CompX<int> complexTwo = MuEr::CompX<int>(10, 15);

	std::cout << (complexOne == complexTwo)<< std::endl;
}

void greaterComplexNumbers(){
	print("Greater Complex Numbers");
	MuEr::CompX<int> complexOne = MuEr::CompX<int>(10, 15);
	MuEr::CompX<int> complexTwo = MuEr::CompX<int>(3 , 2);
	MuEr::CompX<int> complexThree = MuEr::CompX<int>(12 , 18);

	std::cout << (complexOne > complexTwo)<< std::endl;
	std::cout << (complexOne > complexThree)<< std::endl;
}

void lessComplexNumbers() {
	print("Less Than Complex Numbers");
	MuEr::CompX<int> complexOne = MuEr::CompX<int>(10, 15);
	MuEr::CompX<int> complexTwo = MuEr::CompX<int>(3 , 2);
	MuEr::CompX<int> complexThree = MuEr::CompX<int>(12 , 18);

	std::cout << (complexOne < complexTwo)<< std::endl;
	std::cout << (complexOne < complexThree)<< std::endl;
}

void assignComplexNumbers() {
	print("Assignment Operator Complex Numbers");
	MuEr::CompX<int> complexOne = MuEr::CompX<int>(10, 15);
	MuEr::CompX<int> complexTwo = complexOne;

	std::cout << complexTwo << std::endl;
}
